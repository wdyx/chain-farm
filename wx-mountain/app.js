//app.js
var api = require('public/api/api.js');
var util = require('utils/util.js');
var ddic = require('utils/ddic.js');
const { $Message } = require('public/components/iview/base/index');
App({
  onLaunch: function() {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs);

    wx.checkSession({
      success() {
        // session_key 未过期，并且在本生命周期一直有效
      },
      fail() {
        // session_key 已经失效，需要重新执行登录流程
        console.log('session_key 已经失效');

        // 查看是否授权
        wx.getSetting({
          success: res => {            
            if (res.authSetting['scope.userInfo']) {
              
              // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
              wx.getUserInfo({
                success: res => {
                  // 可以将 res 发送给后台解码出 unionId                  
                  // 重新登录
                  api.server.login(res);
                  // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                  // 所以此处加入 callback 以防止这种情况
                  if (this.userInfoReadyCallback != null) {
                    this.userInfoReadyCallback(res)
                  }
                }
              })
            }
          }
        })
      }
    });

    // 获取缓存用户信息
    var _this = this;
    var data = wx.getStorageSync(_this.globalData.localUserKey);
    if(data){      
      _this.globalData.userInfo = data;
    }

    // // 获取定位信息
    // wx.getLocation({
    //   success: function(res) {
    //     console.log(res);
    //   },
    // });

    api.bd.getAddr(function(data){
      console.log(data);
    });
  },
  globalData: {
    userInfo: null,
    api: api,
    version: '1.0.0.1',
    appId: 'wx583daa5229f116a7',
    appSecret: 'ad6acbaed2de4e0ff7e25c0d01777ff9',
    localUserKey: 'slsw-user',
    util:util,
    ddic:ddic,
    message:$Message
  }
})