//获取应用实例
const app = getApp();
 
Component({
  externalClasses: ['i-class'],
  /**
   * 组件的属性列表
   */
  properties: {
    place: {
      type: String,
      value: '获取中...'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isLogin: app.globalData.userInfo == null,
    showCity: false,
    weatherData: '',
    oneChecked: false,
    tags: []
  },

  /**
   * 组件的方法列表
   */
  methods: {

    showCity: function(e) {
      var _this = this;
      var flg = this.data.showCity;
      _this.setData({
        showCity: !flg
      });

      // 下拉状态获取数据
      if (flg) {
        return;
      }
      // 获取数据信息
      getApp().globalData.api.server.ddicCategory('town', function(res) {
        var data = res.data.data;
        _this.setData({
          tags: data
        });
      });
    },
    onChange(event) {
      const detail = event.detail;
      // 绑定事件
      this.triggerEvent('change', {
        detail: detail
      });

      var _this = this;
      var flg = this.data.showCity;
      _this.setData({
        showCity: !flg
      });

    },
    handleClick: function() {
      console.log(222222);
    }
  },
  moveUserInfo: function() {
    console.log(1);
    wx.navigateTo({
      url: '../../pages/index/index'
    })
  }
})