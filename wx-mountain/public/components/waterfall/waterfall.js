

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    praise:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    praise : function(e){
      var _this = this;
      var praise = !_this.data.praise;
      var like = 0;
      if (praise) {
        like = 1;
      } 

      var param = e.currentTarget.dataset      
      var globalData = getApp().globalData;
      var userId = globalData.userInfo.id;
      param.userId = userId;
      param.praise = like;

      console.log(param);
      

      globalData.api.server.exploreRatePraise(param, function (d) {        
        _this.setData({
          praise: praise
        });
      });


     
    }
  }
})
