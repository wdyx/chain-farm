// public/components/topNav/topNav.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    showTopMenu: {
      type: Boolean,
      value: false
    },
    prefix: {
      type: String,
      value: ''
    },
    leftTxt: {
      type: String,
      value: '返回'
    },
    rightTxt: {
      type: String,
      value: '新增'
    },
    rightType: {
      type: String,
      value: ''
    },
    url: {
      type: String,
      value: 'function'
    },
    loadingTxt: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    loading: false,
    current: 'homepage'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    back: function() {
      wx.navigateBack({
        delta: 1,
      })
    },
    click: function() {
      var _this = this;
      _this.setData({
        loading: true
      });

      var url = _this.properties.url;

      if (url == 'function') {

        // 回调函数
        var success = function() {
          _this.setData({
            loading: false
          });
        };


        // 执行函数,绑定事件      
        _this.triggerEvent('click', {
          success
        }, {});

      } else {
        // 跳转页面
        wx.navigateTo({
          url: url,
          success: function(res) {},
          fail: function(res) {
            wx.showToast({
              icon: 'none',
              title: '页面异常...',
            })
          },
          complete: function(res) {
            _this.setData({
              loading: false
            });
          },
        })
      }

    },
    handleChange: function(e) {
      var url = e.detail.url;

      var prefix = this.properties.prefix;

      wx.navigateTo({
        url: prefix + url,
        success: function(res) {},
        fail: function(res) {
          console.log(res);
        },
        complete: function(res) {},
      })
    }
  }
})