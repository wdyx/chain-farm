var config = require('config.js');
var service = config.service;

var http = require('http.js');

var bd = require('baidu.js');

/**
 * 获取用户或者注册用户
 */
function getUserOrRegister(detail, code, cb) {

  var app = getApp();

  // 登录渠道:1:ios手机 2:android手机 3:微信小程序 4:手机H5 5:PC H5
  var channel = '3';

  // 获取系统信息
  const systemInfo = wx.getSystemInfoSync()
  // 获取系统平台
  var os = systemInfo.system;

  // 当前小程序版本
  var version = app.globalData.version;

  // appId
  var appId = app.globalData.appId;
  // appSecret
  var appSecret = app.globalData.appSecret;

  //获取当前时间戳 
  var timestamp = Date.parse(new Date());


  var encryptedData = detail.encryptedData;
  var iv = detail.iv;
  var rawData = detail.rawData;
  var signature = detail.signature;


  // 参数
  var params = {
    "appId": appId,
    'appSecret': appSecret,
    "timestamp": timestamp,
    "version": version,
    "os": os,
    "code": code,
    "encryptedData": encryptedData,
    "iv": iv,
    "rawData": rawData,
    "signature": signature,
    "channel": channel
  };


  http.postReq(service.user.info, params, function(user) {

    var result = user.data;

    var code = result.code;
    if (code == 0) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
    cb(result);
  });

}

var api = {
  login: function(res) {
    var detail = null;
    if(res.detail == undefined){
      detail = res;
    } else {
      detail = res.detail;
    }
    
    // 获取用户信息
    const userInfo = detail.userInfo
    // 授权失败
    if (userInfo == undefined) {
      wx.showToast({
        title: '授权失败！',
        icon: 'none',
        mask: false,
      })
      return false;
    }
    var _this = this;
    // 登录
    wx.login({
      success: res1 => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var app = getApp();
        var code = res1.code;
        getUserOrRegister(detail, code, function(result) {
          var user = null;
          var data = result.data;
          if (data instanceof Array) {
            // 用户已存在返回用户信息
            user = data[0];
          } else {
            // 新增返回用户ID
            userInfo['id'] = data;
            user = userInfo;
          }
          app.globalData.userInfo = user;
          // 缓存用户
          wx.setStorageSync(app.globalData.localUserKey, user);
        });


        
      }
    });
    return true;
  },
  say: function() {
    console.log('say hello!');
  },
  test: function(cb) {
    http.getReq(service.test, cb);
  },
  ddicCategory: function(category, cb) {
    // 获取字典中分指定分类信息
    http.getReq(service.ddic.category + category, cb);
  },
  addExplore: function(params, cb) {
    // 新增发现信息
    http.postReq(service.explore.add, params, cb);
  },
  getExploreList: function(params, cb) {
    // 获取发现信息列表
    http.postReq(service.explore.list, params, cb);
  },
  getExploreById: function(id, cb) {
    // 获取指定Id的发现信息
    http.getReq(service.explore.byId + id, cb);
  },
  getExploreTabList: function (params, cb) {
    // 获取发现选项卡信息列表(状态=2)
    http.postReq(service.explore.tab, params, cb);
  },
  exploreRatePraise: function (params, cb) {
    // 获取发现选项卡信息列表(状态=2)
    http.postReq(service.exploreRate.praise, params, cb);
  }
};

module.exports = {
  server: api,
  bd: bd
}