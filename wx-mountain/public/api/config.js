var prefix = "/server/public/index.php",
  api = "https://shawozi.top" + prefix,
  config = {
    service: {
      host: api,
      test: api,
      user: {
        byCode: api + '/api/user/code?code=',
        info: api + '/api/user/info'
      },
      ddic: {
        category: api + '/api/ddic/category?category=',
      },
      explore: {
        byId: api + '/api/explore/id?id=',
        add: api + '/api/explore/add',
        remove: api + '/api/explore/delete?id=',
        list: api + '/api/explore/list',
        tab: api + '/api/explore/tab',
      },
      exploreRate:{
        praise: api + '/api/Explorerate/praise'
      }
    }
  };

module.exports = config;