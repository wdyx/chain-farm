// 引用百度地图微信小程序JSAPI模块 
var bmapwx = require('./libs/bmap-wx.min.js');
var ak = 'dGYdH6wUoiKjb8vAlwoM9o3kYWriPDiH';
var BMap = null;
var bd = {
  getWeatherData: function (success) {
    if (BMap == null) {
      BMap = new bmapwx.BMapWX({
        ak: ak
      });
    }

    
    // 错误
    var fail = function(data) {
      console.log(data)
    };

    // // chengg
    // var success = function(data) {
    //   //console.log(data);
    //   return data;
    //   // var weatherData = data.currentWeather[0];
    //   // return '城市：' + weatherData.currentCity + '\n' + 'PM2.5：' + weatherData.pm25 + '\n' + '日期：' + weatherData.date + '\n' + '温度：' + weatherData.temperature + '\n' + '天气：' + weatherData.weatherDesc + '\n' + '风力：' + weatherData.wind + '\n';
    // };

    // 发起weather请求 
    BMap.weather({
      fail: fail,
      success: typeof fail == "function" && success
    });

  },
  getAddr : function(callback){
    if (BMap == null) {
      BMap = new bmapwx.BMapWX({
        ak: ak
      });
    }

    var fail = function (data) {
      console.log(data);
    };
    var success = function (data) {
      //返回数据内，已经包含经纬度  
      console.log(data);
      //使用wxMarkerData获取数据  
      //  = data.wxMarkerData;  
      // wxMarkerData = data.originalData.result.addressComponent.city
      // //把所有数据放在初始化data内  
      // console.log(wxMarkerData)
      // that.setData({
      //   // markers: wxMarkerData,
      //   // latitude: wxMarkerData[0].latitude,  
      //   // longitude: wxMarkerData[0].longitude,  
      //   address: wxMarkerData
      // });
      if (typeof callback  == 'function'){
        callback(data.originalData.result);
      }
      
    }
    // 发起regeocoding检索请求   
    BMap.regeocoding({
      fail: fail,
      success: success
    });  
  }
};

module.exports = bd;