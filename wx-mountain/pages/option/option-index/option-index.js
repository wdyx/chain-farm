// miniprogram/pages/option/option-index/option-index.js
 

var aA = null;
var oDiv = null;

Page({
  // 查询天气
  queryWeather:function(e) {
    wx.navigateTo({
      url: '../option-weather/option-weather',
    })
  },
  // 计算器
  queryCalculator: function (e) {
    wx.navigateTo({
      url: '../option-calculator/option-calculator',
    })
  },
  // 认怂书
  rs : function(e){
    wx.navigateTo({
      url: '../option-rs/option-rs',
    })
  },

  

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
     
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})