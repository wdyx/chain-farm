// pages/option/option-weather/option-weather.js
const {
  $Message
} = require('../../../public/components/iview/base/index');
var globalData = getApp().globalData;
Page({
  actionSheetOpen() {
    this.setData({
      visible1: true
    });
  },
  actionSheetCancel() {
    this.setData({
      visible1: false
    });
  },
  handleClickItem1({
    detail
  }) {
    const index = detail.index + 1;

    $Message({
      content: '点击了选项' + index
    });
  },
  back: function() {
    wx.navigateBack({
      delta: 1,
    })
  },

  /**
   * 页面的初始数据
   */
  data: {
    name: 'name1',
    weatherData: '',
    visible1: false,
    actions1: [{
        name: '选项1',
      },
      {
        name: '选项2'
      },
      {
        name: '去分享',
        icon: 'share',
        openType: 'share'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    globalData.api.bd.getWeatherData(function(data) {
      console.log(data);
      // var weatherData = data.currentWeather[0];
      // weatherData = '城市：' + weatherData.currentCity + '\n' + 'PM2.5：' + weatherData.pm25 + '\n' + '日期：' + weatherData.date + '\n' + '温度：' + weatherData.temperature + '\n' + '天气：' + weatherData.weatherDesc + '\n' + '风力：' + weatherData.wind + '\n';


      _this.setData({
        weatherData: data
      });
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})