var globalData = getApp().globalData;

Page({
  // 自定义函数-start
  showSearchPanel: function(e) {
    var flag = e.currentTarget.dataset.show;
    // var query = wx.createSelectorQuery();   
    // var searchPanel = query.select('#search-panel');

    // var unfold = query.select('#unfold');
    // var packup = query.select('#packup');

    this.setData({
      searchShow: flag
    });

  },
  edit: function(e) {
    console.log(e.currentTarget.dataset.id);
  },
  delete: function(e) {
    console.log(e.currentTarget.dataset.id);
  },
  undo: function(e) {
    console.log(e.currentTarget.dataset.id);
  },
  // 自定义函数-end
  /**
   * 页面的初始数据
   */
  data: {
    searchShow: 'unfold',
    list: [],
    page: 1,
    size: 10,
    count:0,
    colors: ["blue","green","red","yellow","default"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var list = [];
    var _this = this;
    var userId = globalData.userInfo.id;
    var params = {};
    params.userId = userId;
    params.page = _this.data.page;
    params.size = _this.data.size;
    console.log(userId);

    globalData.api.server.getExploreList(params,function(d){
      // console.log('*****************************');
      // console.log(d);
      var data = d.data.data;
     
      _this.setData({
        list: data.list,
        count: data.count
      });
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})