var globalData = getApp().globalData;

Page({
  // 自定义函数-start
  back: function() {
    wx.navigateBack({
      delta: 1,
    })
  },
  radioChange({
    detail = {}
  }) {
    this.setData({
      current: detail.value,
      ["params.category"]: detail.code
    });
  },
  checkboxChange({
    detail
  }) {
    var val = detail.value;

    var flag = globalData.util.check.checkArra(val, 3);
    if (flag) {
      globalData.util.showMsg({
        msg: '亲，活动标签只能选择3个哦~~'
      });
    }


    this.setData({
      activityTagSelected: val,
    });


  },
  setVal: function({
    detail,
    dataset
  }) {
    this.setData({
      [detail.target.dataset.name]: detail.detail.value
    });
  },
  save: function(e) {

    var data = this.data;
    // 验证数据
    var errorArr = [];
    
    globalData.util.validate({
      name: '标题',
      value: data.params.title,      
    }, errorArr);

    globalData.util.validate({
      name: '联系人',
      value: data.params.admin      
    }, errorArr);

    globalData.util.validate({
      name: '联系人电话',
      value: data.params.tel,
      type:'number'
    }, errorArr);

    globalData.util.validate({
      name: '详细地址',
      value: data.params.addr,
    }, errorArr);

    globalData.util.validate({
      name: '详细内容',
      value: data.params.content,
    }, errorArr);

    if (data.params.category == 1){
      var tagLength = data.activityTagSelected.length;
      globalData.util.validate({
        name: '活动标签',
        value: tagLength,
        max: 3,
        min: 1,
        type: 'between'
      }, errorArr);
    }
   

    // 展示错误
    if (errorArr.length > 0) {
      var msg = errorArr.join('<br/>');
      globalData.util.showMsg({
        msg: msg
      });
      e.detail.success();
      return;
    } 

    this.setData({
      ["params.userId"]: globalData.userInfo.id,
      ["params.activityTag"]: data.activityTagSelected.join(',')
    });

    // 保存
    globalData.api.server.addExplore(data.params,function(res){
      console.log(res);
      e.detail.success();
      var result = res.data.code;
      if(result == 200){
        globalData.util.showMsg({
          msg: "发布成功，等待审核"
        });

        setTimeout(function(){
          wx.navigateTo({
            url: '../user-message',
          })
        },2000);        
      } else {
        globalData.util.showError();
      }
    });

   


    // setTimeout(function() {
    //   console.log('保存中...');
    //   e.detail.success();
    // }, 3000);


  },
  // 自定义函数-end
  /**
   * 页面的初始数据
   */
  data: {
    activityTag: [],
    activityTagSelected: [],
    category: [{
      id: 1,
      name: '促销活动',
    }, {
      id: 2,
      name: '便民信息'
    }],
    current: ['便民信息'],
    position: 'left',

    params: {
      category: '2',
      title: '',
      tel: '',
      admin: '',
      addr: '',
      content: '',      
      sort: '',
      userId: '',
      activityTag: '',
      activityInfo: '',
      activityDesc: '',
      status: '1',

    },
    verticalCurrent: 2
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    // 获取活动标签
    globalData.api.server.ddicCategory('activity', function(d) {
      var list = d.data.data;
      _this.setData({
        activityTag: list
      });
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})