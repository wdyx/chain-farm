// miniprogram/pages/explore/explore-index/explore-index.js
// import NumberAnimate from "../../../utils/NumberAnimate.js";
var globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    place: '佛坪',
    imgUrls: [
      '/public/img/1.jpg',
      '/public/img/2.jpg',
      '/public/img/3.jpg'
    ],
    indicatorDots: true,    
    autoplay: true,
    interval: 5000,
    duration: 1000,
    current: 'tab2',
    current_scroll: 'tab2',
    show:true,
    list:[],
    count:0,
    page: 1,
    size: 10
  },
  handleChange({ detail }) {
    var _this = this;
    _this.setData({
      current: detail.key
    });

    setTimeout(function(){
      _this.setData({
        show: false
      });
    },3000);
  },

  handleChangeScroll({ detail }) {
    this.setData({
      current_scroll: detail.key
    });
  },

  moveDetail:function(e){
    // wx.showToast({
    //   // title: e.target.dataset.postid + "",//点到不同view层上，返回值不一样
    //   title: e.currentTarget.dataset.id + "",
    // });

    wx.navigateTo({
      url: '../explore-item/explore-item',
    })
  },

  onChange:function(d){
    // 获取选中的城市列表
    var detail = d.detail.detail.item;

    this.setData({
      place: detail.ddicName
    });

    //
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    // if(globalData.userInfo == null){
      
    //   return;
    // }
    // var userId = globalData.userInfo.id;
    var params = {};
     
    params.page = _this.data.page;
    params.size = _this.data.size;
    console.log(params);

    globalData.api.server.getExploreTabList(params, function (d) {      
      var data = d.data.data;
      console.log(data);

      _this.setData({
        list: data.list,
        count: data.count
      });
    });
     
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var _this = this;
    setTimeout(function () {
      _this.setData({
        show: false
      });
    }, 500);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})