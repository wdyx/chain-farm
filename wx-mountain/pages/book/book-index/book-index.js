// miniprogram/pages/book/book-index/book-index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    infoList: [
      {
        icon:'22222',
        text: '111'
      },
      {
        icon: '2222222222',
        text: '1122222222222222222222222222222221'
      },
      {
        icon: '22333333333333333222',
        text: '1133333333333331'
      }
    ],
    indicatorDots: false,
    autoplay: true,
    interval: 1000,
    duration: 500,
    vertical:true,
    current: 'tab1'
  },
  handleChange({ detail }) {
    var _this = this;
    _this.setData({
      current: detail.key
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})