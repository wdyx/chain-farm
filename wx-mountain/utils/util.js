const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const { $Message } = require('../public/components/iview/base/index');

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


/**
 * 将数组分割成2个一组的数组
 * [{left:Array,right:Array}...]
 */
function splitArrayToLeftRight(arr) {
  var result = [];

  for (var i = 0; i < arr.length;) {
    var left = arr[i];
    left.index = i;

    var rightIndex = i + 1;
    var right = null;
    if (rightIndex < arr.length) {
      right = arr[rightIndex];
      right.index = rightIndex;
    }

    result.push({
      left: left,
      right: right
    });
    i += 2;
  }
  return result;
}

/**
 * 将字符串按照,分割
 */
function splitStr(str){
  var split = ",";
  return str.split(split);
}

/**
 * 验证数组长度
 */
function checkArra(arr,length){
  if(arr.length > length){
   
    return true;
  }
  return false;
}




/**
 * 校验Ip地址格式
 *
 * @param {}
 *            ipvale
 */
function checkIp(ipvale) {
	var regex = /^([1-9]|[1-9]\d|1\d{2}|2[0-1]\d|22[0-3])(\.(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){3}$/;
	var b = regex.test(ipvale);
	return b;
}

/**
 * 是否是由字母或数字组成的字符串
 *
 * @param {}
 *            letVale
 */
function checkLetOrNum(letVale) {
  var regex = /^([a-zA-Z_]{1})([\w]*)$/g;
  var b = regex.test(letVale);
  return b;
}
 
/**
* 取字符串的第index的字符
*
* @param {}
*            source
* @param {}
*            index
*/
function interceptStr(source, index) {
  var temp = source.charAt(index);
  return parseInt(temp);
}
/**
 * 检查字符串中beindex位置到endindex位置之间是否全由targer组成
 *
 * @param {}
 *            source
 * @param {}
 *            targer
 * @param {}
 *            beindex
 * @param {}
 *            endindex
 */
function checkStr(source, targer, beindex, endindex) {
  var flag = false;
  for (var i = beindex; i <= endindex; i++) {
    var temp = source.charAt(i);
    if (targer == temp) {
      flag = true;
    }
  }
  return flag;
}

/**
 * 验证字符串中的某一段是否全为0
 *
 * @param {}
 *            source
 * @param {}
 *            begin
 * @param {}
 *            end
 * @return {}
 */
function checkString(source, begin, end) {
  var regex = /^[0]*$/g;
  var temp = source.substring(begin, end + 1);
  //alert("###temp=="+temp);
  return regex.test(temp);
} 


/**
 * 判断两个字符串是否想等 相等返回true否则返回false
 *
 * @param {}
 *            source
 * @param {}
 *            target
 */
function decideString(source, target) {
  return (source == target) ? true : false;
}


/**
 * 将字符串转换成数字
 * @param val
 * @return
 */
function stringToNumber(val) {
  return Number(val);
}

/**
 * 验证是否是整数或小数
 * @param source
 * @return
 */
function checkIntAndFloat(source) {
  var regex = /^[0-9]+(\.[0-9]+)?$/g;
  return regex.test(source);
}
/**
* 验证是否是整数或只有一位小数点的小数
*
* @param {}
*            source
* @return {}
*/
function checkFloat(source) {
  // var regex=/^[1-9]d*.d{1}|0.d{1}[1-9]d{1}$/g;
  var regex = /^[0-9]+\d*[\.\d]?\d{0,1}$/g;
  return regex.test(source);
}


/**
 * 验证是否两位数以内的正整数
 *
 * @param {}
 *            source
 * @return {}
 */
function checkTwoInt(source) {
  var regex = /^[1-9][0-9]?$/g;  //两位数以内的正整数
  return regex.test(source);
}
/**
 * 验证数字
 *
 * @param {}
 *            source
 * @return {}
 */
function checkNumber(source) {
  var regex = /^(\-|\+)?\d+(\.\d+)?$/;
  return regex.test(source);
}
/**
 * 验证是否全是空格
 *
 * @param {}
 *            source
 * @return {}
 */
function checkAllSpace(source) {
  var regex = /^\s+$/g
  return regex.test(source);
}

/**
 * 判断字符串是否为空，若为空则返回true否则返回false
 * @param source
 * @return true或者false
 * https://blog.csdn.net/u012480620/article/details/52227626
 **/
function isEmpty(source) {
  var str = source.replace(/(^\s*)|(\s*$)/g, "");
  if (str == "" || str.toLowerCase() == "null" || str.length <= 0) {
    return true;
  } else {
    return false;
  }
}

/**
 * 验证是否为电话号码（座机）
 *
 * @param {}
 *            source
 */

function isTelephone(source) {
  var regex = /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/
  return regex.test(source);  //search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1
}

/**
 * 验证是否为手机号码（移动手机）
 *
 * @param {}
 *            source
 */

function isMobilePhone(source) {
  var regex = /^((\(\d{3}\))|(\d{3}\-))?1\d{10}/;
  return regex.test(source);
}

/**
 * 验证是否为电子邮箱
 *
 * @param {}
 *            source
 */
function isEmail(source) {
  var regex = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  if (source.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1) {
    return true;
  } else {
    alert("电子邮箱格式不正确");
    return false;
  }
}

/**
 *
 *验证是否为邮编
 * @param
 *      source
 */
function isZip(source) {
  var regex = /^[1-9]\d{5}$/;
  return regex.test(source);
}

/**
 *
 *验证字符串是否是中文
 *
 **/
function isChines(source) {
  var regex = /^[\u4E00-\u9FA5]+$/;
  return regex.test(source);
}

/**
 生成指定位数的随机整数
 **/
function getRandomNum(count) {
  var arr = new Array;
  var reNum = "";
  for (var i = 0; i < count; i++) {
    arr[i] = parseInt(Math.random() * 10);
    reNum += String(arr[i]);
  }
  return reNum;
}
function random(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

/**
 * 弹出错误提示框，默认展示3秒
 */
function showMsg(option){
  var msg = option.msg || '出现错误';
  var duration = option.duration || 3;
  var type = option.type || 'warning';
  $Message({
    content: msg,
    type: type,
    duration: duration
  });
}

function showError(){
  showMsg({msg:'系统错误，请联系管理员。。。。'});
}

function validate(option,errorArr){
  // 验证字段名称
  var name = option.name || '';
  // 验证字段类型：require，number ，between，email ，max,min,eq
  var type = option.type || 'require';
  var value = option.value || '';

  // var start = option.start || '';
  // var end = option.end || '';

  var max = option.max || '';
  var min = option.min || '';

  var confirm = option.confirm || '';
  
  var prefix = "亲，";
  var suffix = "  o(╯□╰)o....";
  var str = '鸭';  
  var msg = {    
    require: `${prefix}${name}不能为空${str}${suffix}`,
    number: `${prefix}${name}类型错误${str}，为非数字~~${suffix}`,
    email: `${prefix}${name}邮箱格式错误${str}${suffix}`,
    between: `${prefix}${name}最少${min}个最多${str}${max}${suffix}`,
    max: `${prefix}${name}超过最大值${str}${max}${suffix}`,
    min: `${prefix}${name}必须大于最小值${str}${min}${suffix}`,
    eq: `${prefix}${name}与${confirm}不相同${str}${suffix}`
  }

  var errorMsg = msg[type];
  var flg =  false;
  if (errorMsg != undefined) {
    flg = true;
  }

  // require，number ，between，email ，max,min,eq
  if (type == 'require'){
    isEmpty(value) && flg ? errorArr.push(errorMsg) : null;
  } else if (type == 'number') {
    !checkNumber(value) && flg ? errorArr.push(errorMsg) : null;
  } else if (type == 'between') {
    if ((value > 3 || value < min) && flg){
      errorArr.push(errorMsg);
    }    
  } else if (type == 'email') {
    isEmail(value) && flg ? errorArr.push(errorMsg) : null;
  } else if (type == 'max') {
    if (value > 3 && flg) {
      errorArr.push(errorMsg);
    } 
  } else if (type == 'min') {
    if (value < min && flg) {
      errorArr.push(errorMsg);
    } 
  } else if (type == 'eq') {
    if (value != confirm && flg){
      errorArr.push(errorMsg);
    }
  }
}

 

var check = {};
// 校验Ip地址格式
check.checkIp = checkIp;
// 是否是由字母或数字组成的字符串
check.checkLetOrNum = checkLetOrNum;
// 取字符串的第index的字符
check.interceptStr = interceptStr;
// 检查字符串中beindex位置到endindex位置之间是否全由targer组成
check.checkStr = checkStr;
// 验证字符串中的某一段是否全为0
check.checkString = checkString;
// 判断两个字符串是否想等 相等返回true否则返回false
check.decideString = decideString;
// 将字符串转换成数字
check.stringToNumber = stringToNumber;
// 验证是否是整数或小数
check.checkIntAndFloat = checkIntAndFloat;
// 验证是否是整数或只有一位小数点的小数
check.checkFloat = checkFloat;
// 验证是否两位数以内的正整数
check.checkTwoInt = checkTwoInt;
// 验证数字
check.checkNumber = checkNumber;
// 验证是否全是空格
check.checkAllSpace = checkAllSpace;
// 判断字符串是否为空，若为空则返回true否则返回false
check.isEmpty = isEmpty;
// 验证是否为电话号码（座机）
check.isTelephone = isTelephone;
// 验证是否为手机号码（移动手机）
check.isMobilePhone = isMobilePhone;
// 验证是否为电子邮箱
check.isEmail = isEmail;
// 验证是否为电子邮编
check.isZip = isZip;
// 验证是否为中文
check.isChines = isChines;
// 生成指定位数的随机整数
check.getRandomNum = getRandomNum;
// 验证数组长度
check.checkArra = checkArra;




module.exports = {
  formatTime: formatTime,
  splitArrayToLeftRight: splitArrayToLeftRight,
  splitStr: splitStr,
  check: check,
  showMsg:showMsg,
  showError: showError,
  validate: validate
}