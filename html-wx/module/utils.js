layui.define(["layer", "jquery"], function(exports) {
	var $ = layui.$,
		layer = layui.layer,
		laytpl = layui.laytpl,
		setter = layui.setter,
		view = layui.view,
		admin = layui.admin

	//公共业务的逻辑处理可以写在此处，切换任何页面都会执行
	var obj = {

		//domainName : "http://localhost:8099",
		domainName: "http://101.132.150.61:8099",
		shawozi: "https://shawozi.top/server/public/index.php/",
		services: {
			index: {
				list: "api/explore/tab?page=1&size=10"
			},
			admin:{
				exploreList :'/api/web/list0'
			}
		},

		//公共颜色码
		colorsArr: [{
				start: '#3DC3FF',
				end: '#01FFFF'
			},
			{
				start: '#D81F72',
				end: '#FF4699'
			},

			{
				start: '#A04DE2',
				end: '#6958E6'
			},
			{
				start: '#FD3E22',
				end: '#EFBF23'
			},

			{
				start: '#F36093',
				end: '#FF465C'
			},
			{
				start: '#C2E74D',
				end: '#00AE8B'
			},
			{
				start: '#08FF9C',
				end: '#10D093'
			},
			{
				start: '#3DC3FF',
				end: '#01FFFF'
			},
			{
				start: '#B850ED',
				end: '#9062EF'
			},
			{
				start: '#FF604A',
				end: '#FBA071'
			},
			{
				start: '#2E8EFE',
				end: '#78F7FF'
			},
			{
				start: '#FFA500',
				end: '#FFEE00'
			},

			{
				start: '#C4E64C',
				end: '#37E0B1'
			},
			{
				start: '#FFDC75',
				end: '#FFDC75'
			},
			{
				start: '#FF4488',
				end: '#F13EFF'
			},
			{
				start: '#A64CE2',
				end: '#625AE7'
			},
			{
				start: '#5AEBC7',
				end: '#5FC9F8'
			},
			{
				start: '#FFA500',
				end: '#FFEE00'
			},
			{
				start: '#00AAD6',
				end: '#625AE7'
			},
			{
				start: '#E48BDE',
				end: '#FF9262'
			},
			{
				start: '#B2797B',
				end: '#FF4B51'
			}
		],
		//数字转化
		number2Img: function() {
			// 数字转化
			layui.use('number2Img', function() {
				var $ = layui.$;
				$('[data-num-img]').map(function() {
					var val = $(this).data('num-img');
					$(this).html(layui.number2Img(val));
				});
			});
		},
		//日期格式化
		formatDate: function(datetime, fmt) {
			if(parseInt(datetime) == datetime) {
				if(datetime.length == 10) {
					datetime = parseInt(datetime) * 1000;
				} else if(datetime.length == 13) {
					datetime = parseInt(datetime);
				}
			}
			datetime = new Date(datetime);
			var o = {
				"M+": datetime.getMonth() + 1, //月份
				"d+": datetime.getDate(), //日
				"h+": datetime.getHours(), //小时
				"m+": datetime.getMinutes(), //分
				"s+": datetime.getSeconds(), //秒
				"q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
				"S": datetime.getMilliseconds() //毫秒
			};
			if(/(y+)/.test(fmt))
				fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
			for(var k in o)
				if(new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		},
		//base64加密
		Base64: {
			_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
			encode: function(e) {
				var t = "";
				var n, r, i, s, o, u, a;
				var f = 0;
				e = obj.Base64._utf8_encode(e);
				while(f < e.length) {
					n = e.charCodeAt(f++);
					r = e.charCodeAt(f++);
					i = e.charCodeAt(f++);
					s = n >> 2;
					o = (n & 3) << 4 | r >> 4;
					u = (r & 15) << 2 | i >> 6;
					a = i & 63;
					if(isNaN(r)) {
						u = a = 64
					} else if(isNaN(i)) {
						a = 64
					}
					t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
				}
				return t
			},
			decode: function(e) {
				var t = "";
				var n, r, i;
				var s, o, u, a;
				var f = 0;
				e = e.replace(/[^A-Za-z0-9+/=]/g, "");
				while(f < e.length) {
					s = this._keyStr.indexOf(e.charAt(f++));
					o = this._keyStr.indexOf(e.charAt(f++));
					u = this._keyStr.indexOf(e.charAt(f++));
					a = this._keyStr.indexOf(e.charAt(f++));
					n = s << 2 | o >> 4;
					r = (o & 15) << 4 | u >> 2;
					i = (u & 3) << 6 | a;
					t = t + String.fromCharCode(n);
					if(u != 64) {
						t = t + String.fromCharCode(r)
					}
					if(a != 64) {
						t = t + String.fromCharCode(i)
					}
				}
				t = Base64._utf8_decode(t);
				return t
			},
			_utf8_encode: function(e) {
				e = e.replace(/rn/g, "n");
				var t = "";
				for(var n = 0; n < e.length; n++) {
					var r = e.charCodeAt(n);
					if(r < 128) {
						t += String.fromCharCode(r)
					} else if(r > 127 && r < 2048) {
						t += String.fromCharCode(r >> 6 | 192);
						t += String.fromCharCode(r & 63 | 128)
					} else {
						t += String.fromCharCode(r >> 12 | 224);
						t += String.fromCharCode(r >> 6 & 63 | 128);
						t += String.fromCharCode(r & 63 | 128)
					}
				}
				return t
			},
			_utf8_decode: function(e) {
				var t = "";
				var n = 0;
				var r = c1 = c2 = 0;
				while(n < e.length) {
					r = e.charCodeAt(n);
					if(r < 128) {
						t += String.fromCharCode(r);
						n++
					} else if(r > 191 && r < 224) {
						c2 = e.charCodeAt(n + 1);
						t += String.fromCharCode((r & 31) << 6 | c2 & 63);
						n += 2
					} else {
						c2 = e.charCodeAt(n + 1);
						c3 = e.charCodeAt(n + 2);
						t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
						n += 3
					}
				}
				return t
			}
		}
	};

	//对外暴露的接口
	exports('utils', obj);
});