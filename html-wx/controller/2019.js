layui.config({
	base: 'module/'
}).use(['layer', 'element', 'utils', 'view', 'dataFilter', 'carousel', 'form', 'util','table'], function() {
	var myUtil = layui.utils;
	var util = layui.util;
	var laydate = layui.laydate;
	var layer = layui.layer;
	var view = layui.view;
	var dataFilter = layui.dataFilter;
	// 轮播
	var carousel = layui.carousel;
	var form = layui.form;
	var element = layui.element;
	var table = layui.table;

	var data = [{
		'xm':'高JJ',
		'tel':'17762182599',
		'remark':'总管、接待'
	},{
		'xm':'草草',
		'tel':'13572605365',
		'remark':'接待'
	},{
		'xm':'小黑',
		'tel':'13991108760',
		'remark':'接待'
	},{
		'xm':'杨朗朗',
		'tel':'15191908995',
		'remark':''
	},{
		'xm':'邓院长',
		'tel':'13571648951',
		'remark':''
	},{
		'xm':'猫',
		'tel':'18710448221',
		'remark':''
	},{
		'xm':'王涛',
		'tel':'18220508685',
		'remark':''
	},{
		'xm':'张斌',
		'tel':'15221015840',
		'remark':''
	},{
		'xm':'黄局',
		'tel':'13630205327',
		'remark':''
	},{
		'xm':'小川',
		'tel':'15336199864',
		'remark':''
	},{
		'xm':'姜涛',
		'tel':'18292669899',
		'remark':''
	},{
		'xm':'天书',
		'tel':'',
		'remark':''
	},{
		'xm':'周爻爻',
		'tel':'13892649653',
		'remark':''
	},{
		'xm':'蒋涛',
		'tel':'',
		'remark':''
	}];
	
	//执行一个 table 实例
	table.render({
		elem: '#user-list',
		height: 420,
		data: data,
		title: '用户表',
		page: false, //开启分页		
		limit: 20,
		cols: [
			[ //表头
				{
					field: 'xm',
					title: '姓名',
					width: "30%",
					sort: true
				}, {
					field: 'tel',
					title: '电话',
					width: "40%",
					templet: function(d){
						return "<a href='tel:"+d.tel+"'> "+d.tel+"</a>";
					}
				}, {
					field: 'remark',
					title: '备注',
					width: "30%"
				}
			]
		]
	});
	
	//监听头工具栏事件
	table.on('toolbar(test)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id),
			data = checkStatus.data; //获取选中的数据
		switch(obj.event) {
			case 'add':
				layer.msg('添加');
				break;
			case 'update':
				if(data.length === 0) {
					layer.msg('请选择一行');
				} else if(data.length > 1) {
					layer.msg('只能同时编辑一个');
				} else {
					layer.alert('编辑 [id]：' + checkStatus.data[0].id);
				}
				break;
			case 'delete':
				if(data.length === 0) {
					layer.msg('请选择一行');
				} else {
					layer.msg('删除');
				}
				break;
		};
	});
	
	//监听行工具事件
	table.on('tool(test)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
		var data = obj.data //获得当前行数据
			,
			layEvent = obj.event; //获得 lay-event 对应的值
		if(layEvent === 'detail') {
			layer.msg('查看操作');
		} else if(layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); //删除对应行（tr）的DOM结构
				layer.close(index);
				//向服务端发送删除指令
			});
		} else if(layEvent === 'edit') {
			layer.msg('编辑操作');
		}
	});

});