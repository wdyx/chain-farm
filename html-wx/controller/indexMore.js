layui.config({
    base: 'module/'
}).use(['layer', 'element','utils', 'view', 'dataFilter','carousel', 'form','util','laypage'], function() {
    var myUtil = layui.utils;
    var util = layui.util;
    var laydate = layui.laydate;
    var layer = layui.layer;
    var view = layui.view;
    var dataFilter = layui.dataFilter;
    // 轮播
    var carousel = layui.carousel;
    var form = layui.form;

    var laypage = layui.laypage;

    //执行一个laypage实例
    laypage.render({
        elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
        ,count: 50 //数据总数，从服务端得到
    });



    $.ajax({
        type : 'post',
        url :  myUtil.domainName +'/common/ddic/index_info',
        async : true,
        success : function(data) {
            var li = $('.pull-left li');
            var footerlinks = $('#about li');
            $(data).each(function (index,d) {
                li.eq(index).find('a>span').text(d.ddicCode);

                footerlinks.eq(index).find('a').text(d.ddicCode);
            });
        }
    });

    var param = {
        "productClassId":1,
        "pageSize":8,
        "pageNum":1
    };
    $.ajax({
        type : 'post',
        url :  myUtil.domainName +'/product/pagedByProduct',
        contentType: "application/json",
        data:JSON.stringify(param),
        async : true,
        success : function(res) {

            var list = res.data;
            console.log(list);
            view('productListNew').send($('#productNew').html(), dataFilter(list,list.length));

        }
    });

});
