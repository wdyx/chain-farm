layui.config({
	base: 'module/'
}).use(['layer', 'element', 'utils', 'view', 'dataFilter', 'carousel', 'form', 'util','table'], function() {
	var myUtil = layui.utils;
	var util = layui.util;
	var laydate = layui.laydate;
	var layer = layui.layer;
	var view = layui.view;
	var dataFilter = layui.dataFilter;
	// 轮播
	var carousel = layui.carousel;
	var form = layui.form;
	var element = layui.element;
	var table = layui.table;

	 
	//执行一个 table 实例
	table.render({
		elem: '#demo',
		height: 420,
		url:  myUtil.shawozi + myUtil.services.admin.exploreList, //数据接口
		title: '信息列表',
		page: true, //开启分页
		toolbar: 'default', //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
		totalRow: true, //开启合计行
		parseData: function(res) { //res 即为原始返回的数据			 
			return {
				"code": res.code, //解析接口状态
				"msg": res.msg, //解析提示文本
				"count": res.data.count, //解析数据长度
				"data": res.data.list //解析数据列表
			};
		},
		response:{
			 statusCode: 200
		},
		cols: [
			[ //表头
				{
					type: 'checkbox',
					fixed: 'left'
				}, {
					field: 'id',
					title: 'ID',
					width: 40,
					sort: true					
				}, {
					field: 'category',
					title: '分类',
					width: 100,
					templet:function(d){
						var category = d.category;
						var categoryText = "促销活动"; 
						if(category == 1){
							categoryText ="便民信息";
						}
						return categoryText;
					}
				}, {
					field: 'status',
					title: '分类',
					width: 100,
					templet:function(d){
						var status = d.status;
						var statusText = "驳回";
						// 1：审核中 2：审核通过 3 审核不通过）
						if(status == 1){
							statusText ="审核中";
						} else if(status == 2){
							statusText ="通过";
						}
						return statusText;
					}
				}, {
					field: 'title',
					title: '标题',
					width: 150,
					sort: true
				}, {
					field: 'content',
					title: '内容',
					width: 250
				}, {
					field: 'admin',
					title: '联系人',
					width: 150
				}, {
					field: 'tel',
					title: '联系电话',
					width: 120
				}, {
					field: 'activityTag',
					title: '活动标签',
					width: 200,
					sort: true
				}, {
					field: 'activityInfo',
					title: '活动描述',
					width: 250
				}, {
					field: 'activityDesc',
					title: '活动备注',
					width: 250
				}, {
					field: 'createTime',
					title: '创建时间',
					width: 200
				}, {
					field: 'tools',
					title: '操作',
					width: 180,
					fixed: 'right',
					templet: '#barDemo'
				}
			]
		]
	});
	
	//监听头工具栏事件
	table.on('toolbar(test)', function(obj) {
		var checkStatus = table.checkStatus(obj.config.id),
			data = checkStatus.data; //获取选中的数据
		switch(obj.event) {
			case 'add':
				layer.msg('添加');
				break;
			case 'update':
				if(data.length === 0) {
					layer.msg('请选择一行');
				} else if(data.length > 1) {
					layer.msg('只能同时编辑一个');
				} else {
					layer.alert('编辑 [id]：' + checkStatus.data[0].id);
				}
				break;
			case 'delete':
				if(data.length === 0) {
					layer.msg('请选择一行');
				} else {
					layer.msg('删除');
				}
				break;
		};
	});
	
	//监听行工具事件
	table.on('tool(test)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
		var data = obj.data //获得当前行数据
			,
			layEvent = obj.event; //获得 lay-event 对应的值
		if(layEvent === 'reject') {
			layer.msg('驳回');
		} else if(layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); //删除对应行（tr）的DOM结构
				layer.close(index);
				//向服务端发送删除指令
			});
		} else if(layEvent === 'pass') {
			layer.msg('通过');
		}
	});

});