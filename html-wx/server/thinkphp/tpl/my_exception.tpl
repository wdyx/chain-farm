<?php
     header("Content-type:text/html;charset=utf-8");
     
     if(!function_exists('parse_file')){
     	function parse_file($file,$line){
     		return basename($file)."line{$line}";
     	}
     }
     
     
     $file_line = sprintf('%s',parse_file($file,$line));
     
     $main = $message;
     
     $arr = [
     	'file_line' => $file_line,
     	'main' => $main,
     	'error' => '出现错误，请联系管理员'
     ];
     
     echo json_encode($arr);
     
?>