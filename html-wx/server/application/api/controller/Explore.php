<?php


namespace app\api\controller;

/**
 * 发现
 */
class Explore extends Common{
	
	/**
	 * 获取explore_tab表中ID的数据
	 */
	public function id(){
		//1. 接收参数
        $this->datas = $this->params;
        
        // 查询数据库
        $res = db('explore_tab')->where('id', $this->datas['id'])->select();
               
        //3. 返回执行结果
        $this->returnWx($res);
	}
	
	/**
	 * 新增
	 */
	public function add(){
		//1.接收参数
        $this->datas = $this->params;
        
        //2.插入数据并获取ID
        $id = db('explore_tab')->insertGetId($this->datas);
        
        //3. 返回执行结果
        $this->returnWx($id);
	}
	
	/**
	 * 根据主键ID删除
	 */
	public function delete(){
		// 接收参数
        $this->datas = $this->params;
        
        // 删除数据
        $res = db('explore_tab')->where('id', $this->datas['id'])->delete();
        
         // 返回执行结果
        $this->returnWx($res);
	}
		
	/**
	 * 查询列表（查询条件支持title，content，userId）
	 */
	public function list(){
		// 接收参数
        $this->datas = $this->params;
        // 页码
        $page = $this->datas['page'];
        // 每页展示条数
        $size = $this->datas['size'];
        
        // 分页信息
        $limit = '';
        if($page > 0){
        	$limit = ($page - 1)*$size.','.$size;
        } else {
        	$limit = '0,10';
        }
        
        // 查询条件
        $where = '';
        // 标题
        if(!empty($this->datas['title'])){
        	$where = $where." title like '%".$this->datas['title']."%'";
        }
        // 内容
        if(!empty($this->datas['content'])){
        	if($where != ''){
        		$where = $where.'AND ';
        	}
        	$where = $where." content like '%".$this->datas['content']."%'";
        } 
        // 用户ID
        if(!empty($this->datas['userId'])){
        	if($where != ''){
        		$where = $where.'AND ';
        	}
        	$where = $where." userId = '".$this->datas['userId']."'";
        }
        
        // 获取列表
        $list = db('explore_tab')->where($where)->order('createTime desc')->limit($limit)->select();
        // 获取总条数
        $count = db('explore_tab')->where($where)->count();
        
        $return_data['list'] = $list;
        $return_data['count'] = $count;
        
        //返回执行结果
        $this->returnWx($return_data);
	}
	
	
	/**
	 * 获取发现tab选项卡数据
	 */
	public function tab(){
		// 接收参数
        $this->datas = $this->params;
        // 页码
        $page = $this->datas['page'];
        // 每页展示条数
        $size = $this->datas['size'];
        
        // 分页信息
        $limit = '';
        if($page > 0){
        	$limit = ($page - 1)*$size.','.$size;
        } else {
        	$limit = '0,10';
        }
        
         // 查询条件（状态 = 2）
        $where = ' e.status = "2" ';
         
        // 获取列表
        $list = db('explore_tab')->alias('e')->join('wx_explore_tab_rate r','e.id = r.explore_tab_id','left')
        	->field('e.id,e.category,e.title,e.content,count(r.id) count,IFNULL(AVG(r.rate),0) rate ,IFNULL(SUM(r.`praise`),0) `praise`')
        	->where($where)
        	->group('e.id')->order('e.createTime desc')->limit($limit)->select();
         
        // 获取总条数
        $count = db('explore_tab')->alias('e')->where($where)->count();
        
        $return_data['list'] = $list;
        $return_data['count'] = $count;
        
        //返回执行结果
        $this->returnWx($return_data);
                
	}
}
?>