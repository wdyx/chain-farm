<?php


namespace app\api\controller;

class Web extends Common{
	
	/**
	 * 获取所有信息列表
	 */
	public function list0(){
		
		// 接收参数
        $this->datas = $this->params;
        // 页码
        $page = $this->datas['page'];
        // 每页展示条数
        $size = $this->datas['limit'];
        
		
		// 分页信息
        $limit = '';
        if($page > 0){
        	$limit = ($page - 1)*$size.','.$size;
        } else {
        	$limit = '0,10';
        }
        
        // 获取列表
        $list = db('explore_tab')->order('createTime desc')->limit($limit)->select();
        // 获取总条数
        $count = db('explore_tab')->count();
        
        $return_data['list'] = $list;
        $return_data['count'] = $count;
        
        //返回执行结果
        $this->returnWx($return_data);
	}
}
?>