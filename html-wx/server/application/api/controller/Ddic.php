<?php

namespace app\api\controller;

/**
 * 字典信息
 */
class Ddic extends Common{
	/**
	 * 根据字典查询字典信息
	 */
    public function category() {    	
    	 //1. 接收参数
        $this->datas = $this->params;
        
    	$res = db('ddic')->where('ddicCategory', $this->datas['category'])->select();
    	if (!empty($res)) {
            $this->returnMsg(200, '获取数据！', $res);
        } else {
            $this->returnMsg(400, '获取数据失败！');
        }
    }
    
//  public function add(){    	
//  	 //1. 接收参数
//      $this->datas = $this->params;
//      //$this->datas['test'] = time();
//      //2. 往数据库插入文章信息
//      $res = db('test')->insertGetId($this->datas);
//
//      //3. 返回执行结果
//      if (!empty($res)) {
//          $this->returnMsg(200, '新增test表成功！', $res);
//      } else {
//          $this->returnMsg(400, '新增test表失败！');
//      }
//  }
}
?>