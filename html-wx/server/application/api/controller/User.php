<?php
	
namespace app\api\controller;
use app\api\controller\wx\WXBizDataCrypt as WXBizDataCrypt;

class User extends Common{
	
	
	/**
	 * 根据code获取用户信息
	 */
	public function code(){
		
		//1. 接收参数
        $this->datas = $this->params;
        
        // 查询数据库
        $res = db('user')->where('openId', $this->datas['openId'])->select();
               
        //3. 返回执行结果
        $this->returnWx($res);
	}
	
	/**
	 * 获取用户信息
	 */
	public function info(){
		
		//1. 接收参数
        $this->datas = $this->params;
        
        $appId = $this->datas['appId'];
        $appSecret = $this->datas['appSecret'];
        $code = $this->datas['code'];
        $encryptedData = $this->datas['encryptedData'];
        $iv = $this->datas['iv'];
       	
		// 获取sessionKey
		$result = $this->getWxSessionKey($appId,$appSecret,$code);
		
		// 非空验证
		if (empty($result)) {
      		 $this->returnWx($result);
        } 
        // 将数据转换成json格式
        $resultJson = json_decode($result);
		
		// 判断是都存在错误        
        if( isset( $resultJson->errcode) ){
            $this->returnWx(json_decode($result));
        }
		
		// 用户唯一标识
		$openid = $resultJson->openid;
		
		// 查询数据库，判断用户是否存在
        $user = db('user')->where('openId', $openid)->select();
        
        // 用户已存在
        if(!empty($user)){
        	$this->returnWx($user);
        }
		
		// 解析用户信息
		$sessionKey = $resultJson->session_key;
		$pc = new WXBizDataCrypt($appId, $sessionKey);
		$errCode = $pc->decryptData($encryptedData, $iv, $data );
		
		// 解析异常判断
		if ($errCode != 0) {
			$this->returnMsg(500, '解析用户出现错误！', $errCode);    
		}
		
		// $this->returnWx($userData);
		$userData = json_decode($data);
		
//		$this->returnWx($userData); 
		
		// 保存用户
		$params = array(			
				"openId" => $userData->openId,
				"nickname" => $userData->nickName,
				"gender" => $userData->gender,
				"language" => $userData->language,
				"country" => $userData->country,
				"province" => $userData->province,
				"city" => $userData->city,
				"avatarUrl" => $userData->avatarUrl,
				"unionId" => '',
				"watermark" => json_encode($userData->watermark),
				"channel" => $this->datas['channel'],
				"os" => $this->datas['os'],
				"tel" => '',
				"email" => "",
				"version" => $this->datas['version']

			); 
			$userId = db('user')->insertGetId($params);
		 
		$this->returnWx($userId);   
	}
	
		
	public function test(){
		
		$appid = 'wx4f4bc4dec97d474b';
		$sessionKey = 'tiihtNczf5v6AKRyjwEUhQ==';
		
		$encryptedData="CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZM
		                QmRzooG2xrDcvSnxIMXFufNstNGTyaGS
		                9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+
		                3hVbJSRgv+4lGOETKUQz6OYStslQ142d
		                NCuabNPGBzlooOmB231qMM85d2/fV6Ch
		                evvXvQP8Hkue1poOFtnEtpyxVLW1zAo6
		                /1Xx1COxFvrc2d7UL/lmHInNlxuacJXw
		                u0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn
		                /Hz7saL8xz+W//FRAUid1OksQaQx4CMs
		                8LOddcQhULW4ucetDf96JcR3g0gfRK4P
		                C7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB
		                6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns
		                /8wR2SiRS7MNACwTyrGvt9ts8p12PKFd
		                lqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYV
		                oKlaRv85IfVunYzO0IKXsyl7JCUjCpoG
		                20f0a04COwfneQAGGwd5oa+T8yO5hzuy
		                Db/XcxxmK01EpqOyuxINew==";
		
		$iv = 'r7BXXKkLb8qrSNn05n0qiA==';
		
		$pc = new WXBizDataCrypt($appid, $sessionKey);
		$errCode = $pc->decryptData($encryptedData, $iv, $data );
		
		if ($errCode == 0) {
			
			$userData = json_decode($data);
//			$this->returnWx($userData); 
			
			
			$params = array(
//				 "openId":$userData->openId,
//				 "openId":$userData->openId,
//				 "openId":$userData->openId,
//				 "openId":$userData->openId,
//				 "openId":$userData->openId,
//				 "openId":$userData->openId,			
				"openId" => $userData->openId,
				"nickname" => $userData->nickName,
				"gender" => $userData->gender,
				"language" => $userData->language,
				"country" => $userData->country,
				"province" => $userData->province,
				"city" => $userData->city,
				"avatarUrl" => $userData->avatarUrl,
				"unionId" => $userData->unionId,
				"watermark" => json_encode($userData->watermark),
				"channel" => '',
				"os" => '',
				"tel" => '',
				"email" => "14",
				"version" => "1.0.0.1"

			);
//			$this->returnWx($params); 
			$userId = db('user')->insertGetId($params);
		 
			$this->returnWx($userId); 
				    
//		    $this->returnMsg(200, '获取数据成功！', $data);

		} else {
		    $this->returnMsg(500, '获取数据！', $errCode);
		}		
	}
	
 
}
?>