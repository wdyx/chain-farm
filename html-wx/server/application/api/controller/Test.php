<?php

namespace app\api\controller;


class Test extends Common
{
	/**
     * [用户登陆时接口请求的方法]
     * @return [null]
     */
    public function aa()
    {
    	$res = db('test')->select();
    	if (!empty($res)) {
            $this->returnMsg(200, '获取数据！', $res);
        } else {
            $this->returnMsg(400, '获取数据失败！');
        }
    }
    
    public function add(){    	
    	 //1. 接收参数
        $this->datas = $this->params;
        //$this->datas['test'] = time();
        //2. 往数据库插入文章信息
        $res = db('test')->insertGetId($this->datas);

        //3. 返回执行结果
        if (!empty($res)) {
            $this->returnMsg(200, '新增test表成功！', $res);
        } else {
            $this->returnMsg(400, '新增test表失败！');
        }
    }
}
?>