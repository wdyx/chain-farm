layui.config({
    base: 'module/'
}).use(['layer', 'element','utils', 'view', 'dataFilter','carousel', 'form','util'], function() {
    var myUtil = layui.utils;
    var util = layui.util;
    var laydate = layui.laydate;
    var layer = layui.layer;
    var view = layui.view;
    var dataFilter = layui.dataFilter;
    // 轮播
    var carousel = layui.carousel;
    var form = layui.form;


    // $.ajax({
    //     type : 'post',
    //     url :  util.domainName + '/common/publicKey',
    //     async : false,
    //     success : function(data) {
    //         localStorage.setItem('key',data);
    //     }
    // });


    $.ajax({
        type : 'post',
        url :  myUtil.domainName +'/common/ddic/index_info',
        async : true,
        success : function(data) {
            var li = $('.pull-left li');
            var footerlinks = $('#about li');
            $(data).each(function (index,d) {
                li.eq(index).find('a>span').text(d.ddicCode);

                footerlinks.eq(index).find('a').text(d.ddicCode);
            });
        }
    });

    var param = {
        "productClassId":1,
        "pageSize":8,
        "pageNum":1
    };
    $.ajax({
        type : 'post',
        url :  myUtil.domainName +'/product/pagedByProduct',
        contentType: "application/json",
        data:JSON.stringify(param),
        async : true,
        success : function(res) {

            var list = res.data;
            console.log(list);
            view('productListNew').send($('#productNew').html(), dataFilter(list,list.length));

        }
    });

    // // 获取最新商品 top6
    // var list = [];
    // list.push({sale:'-20%',new:'NEW',category:'上衣1',name:'新款上衣',price:998,olgPrice:1024});
    // list.push({sale:'-150%',new:'NEW',category:'下衣2',name:'新款下衣',price:99,olgPrice:199});
    // list.push({sale:'-20%',new:'NEW',category:'上衣3',name:'新款上衣',price:998,olgPrice:1024});
    // list.push({sale:'-150%',new:'NEW',category:'下衣4',name:'新款下衣',price:99,olgPrice:199});
    // list.push({sale:'-20%',new:'NEW',category:'上衣5',name:'新款上衣',price:998,olgPrice:1024});
    // list.push({sale:'-150%',new:'NEW',category:'下衣6',name:'新款下衣',price:99,olgPrice:199});
    // list.push({sale:'-20%',new:'NEW',category:'上衣5',name:'新款上衣',price:998,olgPrice:1024});
    // list.push({sale:'-150%',new:'NEW',category:'下衣6',name:'新款下衣',price:99,olgPrice:199});
    //
    // view('productListNew').send($('#productNew').html(), dataFilter(list,list.length));
    //

    //图片轮播
    carousel.render({
        elem: '#middleCarousel'
        ,width: '100%'
        ,height: '440px'
        ,padding:'1em'
        ,interval: 5000
    });



    //倒计时
    var setCountdown = function(id,thisTimer,serverTime,y, M1, d, H, m, s){
        var endTime = new Date(y, M1||0, d||1, H||0, m||0, s||0); //结束日期
        // console.log(endTime.toLocaleString());
        clearTimeout(thisTimer);
        view(id).send($('#countdown').html(), dataFilter([],0));
        util.countdown(endTime, serverTime, function(date, serverTime, timer){
            var str = date[0] + '天' + date[1] + '时' +  date[2] + '分' + date[3] + '秒';
            thisTimer = timer;
            var item = $('#'+id);
            for (var i = 0;i<date.length;i++){
                item.find('li:eq('+i+')>div h3').text(date[i]);
            }
        });
    };
    var thisTimer1,thisTimer2,thisTimer3;
    setCountdown('times1',thisTimer1,new Date(),2018,11,12);
    setCountdown('times2',thisTimer2,new Date(),2018,11,11,0,0,0);
    setCountdown('times3',thisTimer3,new Date(),2018,12,12);

});
