/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : wx_center

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-01-10 12:11:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wx_activity
-- ----------------------------
DROP TABLE IF EXISTS `wx_activity`;
CREATE TABLE `wx_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '活动名称',
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `permanent` varchar(2) DEFAULT NULL COMMENT '永久活动（0否1是 ）',
  `sale` float DEFAULT NULL COMMENT '折扣',
  `fixedprice` varchar(10) DEFAULT NULL COMMENT '一口价',
  `reduceprice` varchar(255) DEFAULT NULL COMMENT '减价',
  `xforx` varchar(255) DEFAULT NULL COMMENT '买x赠x A,B',
  `presentgoods` varchar(255) DEFAULT NULL COMMENT '赠送商品',
  `grouppurchase` varchar(255) DEFAULT NULL COMMENT '团购',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_activity
-- ----------------------------

-- ----------------------------
-- Table structure for wx_ddic
-- ----------------------------
DROP TABLE IF EXISTS `wx_ddic`;
CREATE TABLE `wx_ddic` (
  `ddicId` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ddicCode` varchar(50) DEFAULT NULL,
  `ddicName` varchar(50) DEFAULT NULL,
  `ddicDesc` varchar(200) DEFAULT NULL,
  `ddicSort` int(11) NOT NULL,
  `ddicCategory` varchar(64) DEFAULT NULL,
  `ddicCategoryName` varchar(50) DEFAULT NULL,
  `ddiCextension` varchar(50) DEFAULT NULL COMMENT '扩展字段',
  PRIMARY KEY (`ddicId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COMMENT='字典表';

-- ----------------------------
-- Records of wx_ddic
-- ----------------------------
INSERT INTO `wx_ddic` VALUES ('00000000001', 'foping', '佛坪', '古道明珠，静美佛牌', '1', 'city', '首页城市分类', null);
INSERT INTO `wx_ddic` VALUES ('00000000002', 'yjz', '袁家庄镇', null, '1', 'town', '乡镇', 'default');
INSERT INTO `wx_ddic` VALUES ('00000000003', 'cjb', '陈家坝镇', null, '2', 'town', null, 'red');
INSERT INTO `wx_ddic` VALUES ('00000000004', 'dhb', '大河坝镇', null, '3', 'town', null, 'blue');
INSERT INTO `wx_ddic` VALUES ('00000000005', 'xch', '西岔河镇', null, '4', 'town', null, 'green');
INSERT INTO `wx_ddic` VALUES ('00000000006', 'yb', '岳坝镇', null, '5', 'town', null, 'yellow');
INSERT INTO `wx_ddic` VALUES ('00000000007', 'cjb1', '长角坝镇', '', '6', 'town', '', 'default');
INSERT INTO `wx_ddic` VALUES ('00000000008', 'sdh', '石墩河镇', '', '7', 'town', '', 'blue');
INSERT INTO `wx_ddic` VALUES ('00000000009', 'smd', '十亩地镇', null, '8', 'town', null, 'red');
INSERT INTO `wx_ddic` VALUES ('00000000010', 'sale', '打折', null, '1', 'activity', '活动', '{sael:x}');
INSERT INTO `wx_ddic` VALUES ('00000000011', 'new', '新品上市', null, '2', 'activity', '活动', '{new:x}');
INSERT INTO `wx_ddic` VALUES ('00000000012', 'reduceprice', '减价', '', '3', 'activity', '活动', '{minus:x}');
INSERT INTO `wx_ddic` VALUES ('00000000013', 'clearancesale', '清仓处理', '', '4', 'activity', '活动', '');
INSERT INTO `wx_ddic` VALUES ('00000000014', 'fixedprice', '一口价', null, '5', 'activity', '活动', '{price:x}');
INSERT INTO `wx_ddic` VALUES ('00000000015', 'grouppurchase', '团购', null, '6', 'activity', '活动', '');
INSERT INTO `wx_ddic` VALUES ('00000000016', 'purchaselimitation', '限购', '', '7', 'activity', '活动', '{num:x}');
INSERT INTO `wx_ddic` VALUES ('00000000017', 'xforx', '买赠', '', '8', 'activity', '活动', '{num:x,for:x}');
INSERT INTO `wx_ddic` VALUES ('00000000018', 'presentgoods', '送礼品', null, '9', 'activity', '活动', null);
INSERT INTO `wx_ddic` VALUES ('00000000019', 'roll', '领劵', null, '10', 'activity', '活动', '{full:a1,minus:a2}');
INSERT INTO `wx_ddic` VALUES ('00000000020', 'groupbooking', '拼团', null, '11', 'activity', '活动', '{num:x,price:x}');
INSERT INTO `wx_ddic` VALUES ('00000000021', null, null, null, '2', null, null, '');
INSERT INTO `wx_ddic` VALUES ('00000000022', null, null, null, '3', null, null, '');
INSERT INTO `wx_ddic` VALUES ('00000000023', 'module_activity', '促销活动', '', '1', 'module', '模块', null);
INSERT INTO `wx_ddic` VALUES ('00000000024', 'status_1', '审核中', '', '1', 'status', '状态', 'icon-dengdaishenhe');
INSERT INTO `wx_ddic` VALUES ('00000000025', 'status_2', '通过', '', '2', 'status', '状态', 'icon-pass-2-copy');
INSERT INTO `wx_ddic` VALUES ('00000000026', 'status_3', '未通过', '', '3', 'status', '状态', 'icon-shouquanbohui');

-- ----------------------------
-- Table structure for wx_explore_tab
-- ----------------------------
DROP TABLE IF EXISTS `wx_explore_tab`;
CREATE TABLE `wx_explore_tab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(20) DEFAULT NULL COMMENT '分类（促销活动 / 便民信息）',
  `title` varchar(50) NOT NULL COMMENT '标题（10个字）',
  `tel` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `admin` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `addr` varchar(100) DEFAULT NULL COMMENT '地址',
  `content` varchar(220) DEFAULT NULL COMMENT '内容',
  `type` varchar(10) DEFAULT NULL COMMENT '类型 （打折sale，新品上市new，随机立减random，清仓处理）',
  `sort` int(3) DEFAULT NULL COMMENT '排序',
  `userId` int(11) NOT NULL COMMENT '创建用户',
  `activityTag` varchar(50) DEFAULT NULL COMMENT '活动标签',
  `activityInfo` varchar(100) DEFAULT NULL,
  `activityDesc` varchar(100) DEFAULT NULL COMMENT '活动描述',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '当前状态（默认状态1：审核中 2：审核通过 3 审核不通过）',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `explore_activity_index` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_explore_tab
-- ----------------------------

-- ----------------------------
-- Table structure for wx_rate
-- ----------------------------
DROP TABLE IF EXISTS `wx_rate`;
CREATE TABLE `wx_rate` (
  `rateId` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论ID',
  `sort` int(3) DEFAULT NULL COMMENT '排序',
  `time` datetime DEFAULT NULL COMMENT '评论时间',
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `content` varchar(50) DEFAULT NULL COMMENT '内容',
  `relevanceId` int(11) NOT NULL COMMENT '关联Id',
  `moduleId` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`rateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_rate
-- ----------------------------

-- ----------------------------
-- Table structure for wx_test
-- ----------------------------
DROP TABLE IF EXISTS `wx_test`;
CREATE TABLE `wx_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_test
-- ----------------------------
INSERT INTO `wx_test` VALUES ('1', 'AA');
INSERT INTO `wx_test` VALUES ('2', 'BB ');
INSERT INTO `wx_test` VALUES ('3', 'CC');
INSERT INTO `wx_test` VALUES ('4', 'DD');
INSERT INTO `wx_test` VALUES ('5', '111');
INSERT INTO `wx_test` VALUES ('6', 'wwwwwww');

-- ----------------------------
-- Table structure for wx_user
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `openId` char(28) NOT NULL COMMENT '小程序用户openid',
  `nickname` varchar(100) NOT NULL COMMENT '用户昵称',
  `gender` varchar(100) NOT NULL COMMENT '性别   1男   0女  2未知',
  `language` varchar(10) DEFAULT NULL COMMENT '语言',
  `country` varchar(10) DEFAULT '' COMMENT '所在国家',
  `province` varchar(50) DEFAULT NULL COMMENT '省份',
  `city` varchar(30) DEFAULT NULL COMMENT '城市',
  `avatarUrl` varchar(300) DEFAULT NULL COMMENT '用户头像',
  `unionId` varchar(50) DEFAULT NULL COMMENT 'unionId',
  `watermark` varchar(200) NOT NULL COMMENT '"watermark":JSON',
  `channel` varchar(10) DEFAULT NULL COMMENT '注册渠道 1:ios手机 2:android手机 3:微信小程序 4:手机H5 5:PC H5',
  `os` varchar(10) DEFAULT NULL COMMENT '注册平台 安卓 苹果',
  `tel` varchar(15) DEFAULT NULL COMMENT '注册电话',
  `email` varchar(50) DEFAULT NULL COMMENT '注册邮箱',
  `enabled` tinyint(1) DEFAULT '1' COMMENT '是否激活 1：激活 0：冻结',
  `version` varchar(10) DEFAULT NULL COMMENT ' 版本',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx_user
-- ----------------------------
INSERT INTO `wx_user` VALUES ('00000000006', 'oxBwH0bxMYP0gw36PrPQs8Wkgi1g', '仅此一ღ', '1', 'zh_CN', 'China', 'Shaanxi', 'Xi\'an', 'https://wx.qlogo.cn/mmopen/vi_32/kLccyOq02zCl0cX7mJhqAE8Csg1RpRrichw6iclZULzva9bg1Xva0cMNSYvYViaPB0XTOZE7Wu5cXWTZTaqLiawicibg/132', '', '{\"timestamp\":1546844805,\"appid\":\"wx583daa5229f116a7\"}', '3', 'Android 8.', '', '', '1', '1.0.0.1', null);
