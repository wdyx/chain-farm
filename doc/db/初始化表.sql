/*==============================================================*/
/* table: sys_ddic   系统表-数据字典                              */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_ddic`;
CREATE TABLE `sys_ddic` (
  `ddicId` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ddicCode` varchar(50) DEFAULT NULL,
  `ddicName` varchar(50) DEFAULT NULL,
  `ddicDesc` varchar(200) DEFAULT NULL,
  `ddicSort` int(11) NOT NULL,
  `ddicCategory` varchar(64) DEFAULT NULL,
  `ddicCategoryName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ddicId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- SET FOREIGN_KEY_CHECKS=1;


