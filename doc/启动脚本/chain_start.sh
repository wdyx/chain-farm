#! /bin/bash

geth --identity "my eth" --rpc --rpccorsdomain "*" --datadir "./data" --port "30303" --rpcapi "db,eth,net,w3j" --networkid 95518 --rpcport 8545 --rpcaddr 0.0.0.0 console 2>> ./logs/geth.log
