#! /bin/bash  
PROC_NAME=$1  
ProcNumber=`ps -ef |grep -w $PROC_NAME | grep -v grep|wc -l`  
if [ $ProcNumber -le 0 ];then  
   result=0  
   echp "进程已停止即将重启"
   java -jar -Xms80m $1 >logs/$1-log.log 2>&1 &

else  
   result=1   
   echo "进程已正常运行"
fi
