package com.wechat;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.thymeleaf.TemplateEngine;

/**
 *
 */
@SpringBootApplication
@MapperScan("com.wechat.module.*.dao")
public interface WechatApiApplication {

    public static void main(String[] args) {

        //  使用Thymeleaf的layout:fragment
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.addDialect(new LayoutDialect());

        SpringApplication.run(WechatApiApplication.class, args);
    }


}