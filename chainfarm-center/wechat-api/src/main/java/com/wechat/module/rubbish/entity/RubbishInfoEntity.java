package com.wechat.module.rubbish.entity;

import lombok.Data;

import java.util.Date;

/*
*垃圾信息
*@Author ziming
*@Date 2019/7/7 14:10
*/
@Data
public class RubbishInfoEntity {

    private Integer id;

    private String rubbishName;

    private Integer typeId;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;
}
