package com.wechat.module.rubbish.service;

import com.wechat.module.rubbish.dao.MenuTypeDao;
import com.wechat.module.rubbish.entity.MenuTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuTypeService {

    @Autowired
    private MenuTypeDao menuTypeDao;

    /*
    *查询全部分类
    *@Author ziming
    *@Date 2019/7/7 14:27
    */
    public List<MenuTypeEntity> getAll() {
        return menuTypeDao.getAll();
    }
}
