package com.wechat.module.rubbish.controller;

import com.wechat.module.rubbish.entity.RubbishInfoEntity;
import com.wechat.module.rubbish.service.RubbishInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author ziming
 * @Date 2019/7/7 14:18
 */
@RestController
@RequestMapping("/rubbishInfo")
public class RubbishInfoController {

    @Autowired
    private RubbishInfoService rubbishInfoService;

    @PostMapping("/getRubbishByName")
    public List<RubbishInfoEntity> getRubbishByName(@RequestParam("rubbishName") String rubbishName,
        @RequestParam("typeId") Integer typeId) {
        return rubbishInfoService.getRubbishByName(rubbishName, typeId);
    }

}
