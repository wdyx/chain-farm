package com.wechat.module.rubbish.service;

import com.wechat.module.rubbish.dao.RubbishInfoDao;
import com.wechat.module.rubbish.entity.RubbishInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RubbishInfoService {

    @Autowired
    private RubbishInfoDao rubbishInfoDao;

    /*
    * 查询垃圾信息
    *@Author ziming
    *@Date 2019/7/7 14:27
    */
    public List<RubbishInfoEntity> getRubbishByName(String rubbishName, Integer typeId) {
        return rubbishInfoDao.getRubbishByName(rubbishName, typeId);
    }
}
