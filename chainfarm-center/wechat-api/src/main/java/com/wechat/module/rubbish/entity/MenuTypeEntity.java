package com.wechat.module.rubbish.entity;

import lombok.Data;

import java.util.Date;

/*
*菜单类型
*@Author ziming
*@Date 2019/7/7 14:10
*/
@Data
public class MenuTypeEntity {

    private Integer id;

    private String typeName;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;
}
