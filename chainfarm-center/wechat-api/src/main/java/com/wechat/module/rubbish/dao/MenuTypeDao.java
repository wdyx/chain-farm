package com.wechat.module.rubbish.dao;

import com.wechat.module.rubbish.entity.MenuTypeEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
*查询菜单
*@Author ziming
*@Date 2019/7/7 14:13
*/
@Repository
public interface MenuTypeDao {

    List<MenuTypeEntity> getAll();
}
