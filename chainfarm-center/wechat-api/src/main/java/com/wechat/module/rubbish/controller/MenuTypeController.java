package com.wechat.module.rubbish.controller;

import com.wechat.module.rubbish.entity.MenuTypeEntity;
import com.wechat.module.rubbish.service.MenuTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author ziming
 * @Date 2019/7/7 14:18
 */
@RestController
@RequestMapping("/menuType")
public class MenuTypeController {

    @Autowired
    private MenuTypeService menuTypeService;

    @GetMapping("/getAll")
    public List<MenuTypeEntity> getAll() {
        return menuTypeService.getAll();
    }

}
