package com.wechat.module.rubbish.dao;

import com.wechat.module.rubbish.entity.RubbishInfoEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
*搜索垃圾信息
*@Author ziming
*@Date 2019/7/7 14:13
*/
@Repository
public interface RubbishInfoDao {

    List<RubbishInfoEntity> getRubbishByName(@Param("rubbishName") String rubbishName, @Param("typeId") Integer typeId);
}
