package com.wechat.module.comm;

import lombok.Data;

@Data
public class CommRequest {

    private Long pageSize = 20L;//每页条数

    private Long pageNum = 1L;//当前页
}
