package com.wechat.config.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * 响应结果封装
 */
public class ResultGenerator {

    public static Result genSuccessResult() {
        Result result= new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        return result;
    }

    public static Result genSuccessResult(Object data) {
        Result result= new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }

    public static Result genFailResult(Exception e) {
        String msg = e.getMessage();
        if(StringUtils.isBlank(msg)){
            msg = ResultCode.FAIL.getMsg();
        }

        Result result= new Result();
        result.setCode(ResultCode.FAIL.getCode());
        result.setMsg(msg);
        return result;
    }

    public static Result genErrorResult(int code,String msg) {
        Result result= new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
