package com.wechat.config.exception;


/**
 * 定义响应状态码枚举类
 */
public enum ResultCode {

    SUCCESS(200,"成功"),
    FAIL(400,"失败"),
    UNAUTHORIZED(401,"未认证，签名错误"),
    NOT_FOUND(404,"接口不存在"),
    PARAM_ERROR(1001,"参数错误"),
    INTERNAL_SERVER_ERROR(500,"服务器内部错误");

    private  int code;

    private  String msg;



    private ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 获取值对应的枚举对象
     *
     * @param value
     * @return
     */
    public static ResultCode getInstance(int value) {
        for (ResultCode obj : ResultCode.values()) {
            if (obj.getCode() == value) {
                return obj;
            }
        }
        return null;
    }
}
