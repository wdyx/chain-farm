package com.farm.model.comm;

import lombok.Data;

import java.util.Date;

@Data
public class CommEntity {

    private Date createTime;

    private Date updateTime;

    private Integer deleted;
}
