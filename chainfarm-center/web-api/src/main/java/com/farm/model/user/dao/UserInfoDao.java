package com.farm.model.user.dao;

import com.farm.model.user.entity.UserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
*系统分类
*@Author ziming
*@Date 2018/11/7 15:43
*/
@Repository
public interface UserInfoDao {

    List<UserInfo> getUserInfoBy(Map map);

}
