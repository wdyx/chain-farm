package com.farm.model.user.controller;

import com.farm.config.exception.Result;
import com.farm.config.exception.ResultGenerator;
import com.farm.model.user.entity.UserInfo;
import com.farm.model.user.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /**
    *条件查询用户接口
    *@Author ziming
    *@Date 2018/11/12 21:25
    */
    @GetMapping(value = "/getUserInfoBy")
    @ResponseBody
    public Result getUserInfoBy(@RequestParam(value = "mobile") String mobile,
        @RequestParam(value = "openId") String openId){
        List<UserInfo> userInfoList = userInfoService.getUserInfoBy(mobile,openId);
        return ResultGenerator.genSuccessResult(userInfoList);
    }

}
