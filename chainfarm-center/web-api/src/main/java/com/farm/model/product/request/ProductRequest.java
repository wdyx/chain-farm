package com.farm.model.product.request;

import com.farm.model.comm.CommRequest;
import lombok.Data;

@Data
public class ProductRequest extends CommRequest{

    private Integer productClassId;//分类id

}
