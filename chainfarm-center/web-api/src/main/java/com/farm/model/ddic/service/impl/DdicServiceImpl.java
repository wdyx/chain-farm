package com.farm.model.ddic.service.impl;

import com.farm.model.ddic.dao.DdicDao;
import com.farm.model.ddic.entity.Ddic;
import com.farm.model.ddic.service.DdicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DdicServiceImpl implements DdicService {

    @Autowired
    private DdicDao ddicDao;

    /**
     * 按照字典分类获取字典列表
     * @return
     */
    @Override
    public List<Ddic> getCategoryList(String ddicCategory) throws Exception {
        // 验证
        if(StringUtils.isBlank(ddicCategory)){
            throw new Exception("参数异常");
        }
        return ddicDao.getCategoryList(ddicCategory);
    }
}
