package com.farm.model.sys.dao;

import com.farm.model.sys.entity.SysClass;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
*系统分类
*@Author ziming
*@Date 2018/11/7 15:43
*/
@Repository
public interface SysClassDao {

    List<SysClass> getSysClassBy(Map map);

}
