package com.farm.model.product.dao;

import com.farm.model.product.entity.Product;
import com.farm.model.product.request.ProductRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDao {

    List<Product> pagedByProduct(ProductRequest productRequest);

    void addProduct(Product product);
}
