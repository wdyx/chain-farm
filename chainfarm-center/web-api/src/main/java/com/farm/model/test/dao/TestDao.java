package com.farm.model.test.dao;

import com.farm.model.test.enetity.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TestDao {

    @Select("select * from test")
    public List<Test> getAll();
}
