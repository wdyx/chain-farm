package com.farm.model.message.controller;

import com.farm.config.exception.PageResult;
import com.farm.config.exception.Result;
import com.farm.config.exception.ResultCode;
import com.farm.config.exception.ResultGenerator;
import com.farm.model.message.entity.MessageInfo;
import com.farm.model.message.request.MsgInfoRequest;
import com.farm.model.message.service.MessageInfoService;
import com.farm.model.product.entity.Product;
import com.farm.model.product.request.ProductRequest;
import com.farm.model.product.request.TaobaoPullRequest;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/messageInfo")
public class MessageInfoController {

    @Autowired
    private MessageInfoService messageInfoService;



    /**
    *分页查询消息
    *@Author ziming
    *@Date 2018/11/12 21:48
    */
    @RequestMapping(value = "/pagedByMessageInfo", method = RequestMethod.POST)
    @ResponseBody
    public PageResult pagedByMessageInfo(@RequestBody MsgInfoRequest msgInfoRequest) {
        PageHelper.startPage(msgInfoRequest.getPageNum().intValue(),msgInfoRequest.getPageSize().intValue());
        List<MessageInfo> messageInfoList = messageInfoService.pagedByMessageInfo(msgInfoRequest);
        PageInfo<MessageInfo> pageInfo= new PageInfo<>(messageInfoList);
        PageResult result = new PageResult();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setTotalSize(pageInfo.getTotal());
        result.setData(messageInfoList);
        return result;
    }


    /**
    *发布消息
    *@Author ziming
    *@Date 2018/11/13 11:50
    */
    @PostMapping(value = "/addMessageInfo")
    @ResponseBody
    public Result addMessageInfo(@RequestBody MsgInfoRequest msgInfoRequest){
        if(Objects.isNull(msgInfoRequest) || StringUtils.isBlank(msgInfoRequest.getContent())){
            return ResultGenerator.genErrorResult(ResultCode.PARAM_ERROR.getCode(),ResultCode.PARAM_ERROR.getMsg());
        }
        messageInfoService.addMessageInfo(msgInfoRequest);
        return  ResultGenerator.genSuccessResult();
    }

}
