package com.farm.model.ddic.entity;

import lombok.Data;

@Data
public class Ddic {

    private int ddicId;
    private String ddicCode;
    private String ddicName;
    private String ddicDesc;
    private String ddinticSort;
    private String ddicCategory;
    private String ddicCategoryName;

}
