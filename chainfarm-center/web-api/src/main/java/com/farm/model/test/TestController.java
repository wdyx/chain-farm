package com.farm.model.test;


import com.farm.model.test.dao.TestDao;
import com.farm.model.test.enetity.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private TestDao testDao;

    @RequestMapping("testAA")
    public List<Test> getAll(){
        return testDao.getAll();
    }
    @RequestMapping("/test/{name}")
    public String sayHell(@PathVariable String name){
        return name+",hello!";
    }
}
