package com.farm.model.user.entity;

import com.farm.model.comm.CommEntity;
import lombok.Data;

/**
 * 用户
 */
@Data
public class UserInfo extends CommEntity {

    private Integer userId;

    private Integer cityId;

    private String address;

    private String headUrl;

    private String nickName;

    private String mobile;

    private String openId;

    private String remark;

    private Integer sex;

}
