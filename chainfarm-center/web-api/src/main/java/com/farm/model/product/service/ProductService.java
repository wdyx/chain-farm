package com.farm.model.product.service;

import com.farm.model.product.dao.ProductDao;
import com.farm.model.product.entity.Product;
import com.farm.model.product.request.ProductRequest;
import com.farm.model.product.request.TaobaoPullRequest;
import com.farm.model.taobao.service.TaobaoService;
import com.taobao.api.domain.NTbkItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

	@Autowired
	private ProductDao productDao;
	@Autowired
	private TaobaoService taobaoService;


	public List<Product> pagedByProduct(ProductRequest productRequest){
		return  productDao.pagedByProduct(productRequest);
	}

	public void addProductToTaobao(TaobaoPullRequest taobaoPullRequest){
		List<NTbkItem> nTbkItemList = taobaoService.invokingTaobao(taobaoPullRequest);
		if(nTbkItemList.size() <= 0){
			return;
		}
		for(NTbkItem nTbkItem:nTbkItemList){
			Product product = new Product();
			product.setProductNo(nTbkItem.getNumIid().toString());
			product.setProductClassId(taobaoPullRequest.getProductClassId());
			product.setProductName(nTbkItem.getTitle());
			product.setProductUrl(nTbkItem.getItemUrl());
			product.setPicUrl(nTbkItem.getPictUrl());
			product.setReservePrice(nTbkItem.getReservePrice());
			product.setZkFinalPrice(nTbkItem.getZkFinalPrice());
			productDao.addProduct(product);
		}
	}

}
