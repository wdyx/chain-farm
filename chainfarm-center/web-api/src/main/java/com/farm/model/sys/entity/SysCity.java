package com.farm.model.sys.entity;

import com.farm.model.comm.CommEntity;
import lombok.Data;

/**
 * 城市
 */
@Data
public class SysCity extends CommEntity {

    private Integer cityId;

    private String cityName;

    private String postCode;

    private String locationCode;

    private Integer parentId;

}
