package com.farm.model.sys.controller;

import com.farm.config.exception.Result;
import com.farm.config.exception.ResultGenerator;
import com.farm.model.sys.entity.SysClass;
import com.farm.model.sys.service.SysClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/sysClass")
public class SysController {

    @Autowired
    private SysClassService sysClassService;

    /**
    *查询分类
    *@Author ziming
    *@Date 2018/11/7 18:52
    */
    @GetMapping(value = "/getSysClassBy")
    @ResponseBody
    public Result getSysClassBy(@RequestParam(value = "classSex",defaultValue = "0") Integer classSex,
        @RequestParam(value = "classType",defaultValue = "0") Integer classType){
        List<SysClass> sysClassList = sysClassService.getSysClassBy(classSex,classType);
        return ResultGenerator.genSuccessResult(sysClassList);
    }

}
