package com.farm.model.sys.entity;

import com.farm.model.comm.CommEntity;
import lombok.Data;

/**
 * 系统分类
 */
@Data
public class SysClass extends CommEntity {

    private Integer classId;

    private String className;

    /**
    * 分类性别 （0位置  1男 2女）
    */
    private Integer classSex;

    /**
     * 分类类型（0未知 1.商品 2.信息）
     */
    private Integer classType;

}
