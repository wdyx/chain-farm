package com.farm.model.ddic.dao;

import com.farm.model.ddic.entity.Ddic;
import com.farm.model.test.enetity.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DdicDao {
    /**
     * 按照字典分类获取字典列表
     * @return
     */
    @Select("SELECT * from sys_ddic where  ddicCategory = #{ddicCategory}")
    public List<Ddic> getCategoryList(String ddicCategory);
}
