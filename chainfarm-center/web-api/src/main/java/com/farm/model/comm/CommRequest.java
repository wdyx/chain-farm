package com.farm.model.comm;

import lombok.Data;

import java.util.Date;

@Data
public class CommRequest {

    private Long pageSize = 20L;//每页条数

    private Long pageNum = 1L;//当前页
}
