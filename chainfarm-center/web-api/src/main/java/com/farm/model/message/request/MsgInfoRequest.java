package com.farm.model.message.request;

import com.farm.model.comm.CommRequest;
import lombok.Data;

@Data
public class MsgInfoRequest extends CommRequest{

    private Integer  userId;

    private Integer classId;

    private Integer cityId;

    private String content;

    private String telephone;

}
