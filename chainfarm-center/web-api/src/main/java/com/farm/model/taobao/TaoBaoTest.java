package com.farm.model.taobao;

import com.alibaba.fastjson.JSON;
import com.farm.model.product.service.ProductService;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.domain.NTbkItem;
import com.taobao.api.request.TbkItemGetRequest;
import com.taobao.api.response.TbkItemGetResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
*
*@Author ziming
*@Date 2018/11/2 15:46
*/
public class TaoBaoTest {

    @Autowired
    private ProductService productService;

    public static void main(String[] args) throws Exception {


        //http://open.taobao.com/api.htm?docId=24515&docType=2  文档地址
        // TOP服务地址，正式环境需要设置为http://gw.api.taobao.com/router/rest
        //http://gw.api.tbsandbox.com/router/rest
        String serverUrl = "http://gw.api.taobao.com/router/rest";
        String appKey = "25255015"; // 可替换为您的沙箱环境应用的AppKey
        String appSecret = "9bf8fd54cf5131068f3ff4150ab60c48"; // 可替换为您的沙箱环境应用的AppSecret
        String sessionKey = "6100a26f64862e9373c5be53e26869a99d165db8907480e92220917"; // 必须替换为沙箱账号授权得到的真实有效SessionKey

        TaobaoClient client = new DefaultTaobaoClient(serverUrl, appKey, appSecret);

        TbkItemGetRequest req = new TbkItemGetRequest();
        req.setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,seller_id,volume,nick");
        req.setQ("女装");
        //req.setCat("16,18");
        //req.setItemloc("杭州");
//        req.setSort("tk_rate_des");
//        req.setIsTmall(false);
//        req.setIsOverseas(false);
//        req.setStartPrice(10L);
//        req.setEndPrice(10L);
//        req.setStartTkRate(5000L);
//        req.setEndTkRate(100L);
//        req.setPlatform(1L);
        req.setPageNo(1L);
        req.setPageSize(20L);
        TbkItemGetResponse rsp = client.execute(req);
        System.out.println(rsp.getBody());
        List<NTbkItem> nTbkItems = rsp.getResults();
        System.out.println("nTbkItems++++++" + JSON.toJSON(nTbkItems));
        //https://oauth.taobao.com/authorize?response_type=code&client_id=25255015&redirect_uri=http://www.51tttj.com/2/&state=1212&view=web
        //5EWY1boZEZus5BDG7i9wt9EX905872

        //获取seesionkey  access_token=6100a26f64862e9373c5be53e26869a99d165db8907480e92220917
       // https://oauth.taobao.com/authorize?response_type=token&client_id=25255015&state=1212&view=web

//        https://oauth.taobao.com/token?client_id=25255015&client_secret=9bf8fd54cf5131068f3ff4150ab60c48&grant_type=authorization_code&code=5EWY1boZEZus5BDG7i9wt9EX905872&redirect_uri=http://www.51tttj.com/2/
    }

    private static final Pattern DECIMAL_PATTERN = Pattern.compile("([1-9]\\d*\\.?\\d*)|(0\\.\\d*[1-9])");

    public static boolean isDecimal(String margin) {
        Matcher m = DECIMAL_PATTERN.matcher(margin);
        return m.matches();
    }



}
