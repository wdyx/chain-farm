package com.farm.model.ddic.service;

import com.farm.model.ddic.entity.Ddic;

import java.util.List;

public interface DdicService {
    /**
     * 按照字典分类获取字典列表
     * @return
     */
    public List<Ddic> getCategoryList(String ddicCategory) throws Exception;
}
