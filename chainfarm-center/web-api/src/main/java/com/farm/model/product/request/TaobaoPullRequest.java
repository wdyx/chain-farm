package com.farm.model.product.request;

import com.farm.model.comm.CommRequest;
import lombok.Data;

@Data
public class TaobaoPullRequest extends CommRequest{

    private String cat; //后台类目ID，用,分割，最大10个，该ID可以通过taobao.itemcats.get接口获取到

    private String searchName;//搜索词

    private String itemloc;//所在地

    private String  sort;//排序_des（降序），排序_asc（升序），销量（total_sales），淘客佣金比率（tk_rate）， 累计推广量（tk_total_sales），总支出佣金（tk_total_commi） 如tk_rate_des

    private Boolean isTmall = false;//是否商城商品，设置为true表示该商品是属于淘宝商城商品，设置为false或不设置表示不判断这个属性

    private Boolean isOverseas = false;//是否海外商品，设置为true表示该商品是属于海外商品，设置为false或不设置表示不判断这个属性

    private Integer startPrice;//折扣价范围下限，单位：元

    private Integer endPrice;//折扣价范围上限，单位： 元

    private Integer startTkRate;//淘客佣金比率上限，如：1234表示12.34%

    private Integer endTkRate;//淘客佣金比率下限，如：1234表示12.34%

    private Integer platform;//链接形式：1：PC，2：无线，默认：１

    private Integer productClassId;//商品分类id  自己系统的


}
