package com.farm.model.view.controller;

import com.farm.model.product.entity.Product;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 首页借口
 */
@RestController
@RequestMapping("index")
public class IndexController {

    /**
     * 获取首页"hot"TOP3商品
     * @return
     */
    public List<Product> getHotList(){

        return null;
    }
}
