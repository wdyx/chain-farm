package com.farm.model.comm;

import java.util.ArrayList;
import java.util.List;

public class Const {

    public static List<String> ignoreUrl = new ArrayList<>();

    static {
        ignoreUrl.add("/common/publicKey");
        ignoreUrl.add("/error");
    }
}
