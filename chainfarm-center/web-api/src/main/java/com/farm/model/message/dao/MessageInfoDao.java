package com.farm.model.message.dao;

import com.farm.model.message.entity.MessageInfo;
import com.farm.model.message.request.MsgInfoRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
*消息内容
*@Author ziming
*@Date 2018/11/7 15:43
*/
@Repository
public interface MessageInfoDao {

    List<MessageInfo> pagedByMessageInfo(MsgInfoRequest msgInfoRequest);

    void insertSelective(MessageInfo messageInfo);

}
