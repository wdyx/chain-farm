package com.farm.model.message.service;

import com.farm.model.message.dao.MessageInfoDao;
import com.farm.model.message.entity.MessageInfo;
import com.farm.model.message.request.MsgInfoRequest;
import com.farm.model.product.entity.Product;
import com.farm.model.product.request.TaobaoPullRequest;
import com.google.common.collect.Maps;
import com.taobao.api.domain.NTbkItem;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MessageInfoService {

	@Autowired
	private MessageInfoDao messageInfoDao;

	public List<MessageInfo> pagedByMessageInfo(MsgInfoRequest msgInfoRequest){
		return  messageInfoDao.pagedByMessageInfo(msgInfoRequest);
	}

	public void addMessageInfo(MsgInfoRequest msgInfoRequest){
		MessageInfo messageInfo = new MessageInfo();
		BeanUtils.copyProperties(msgInfoRequest,messageInfo);
		messageInfoDao.insertSelective(messageInfo);
	}
}
