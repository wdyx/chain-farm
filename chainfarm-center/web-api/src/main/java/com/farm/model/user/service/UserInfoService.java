package com.farm.model.user.service;

import com.farm.model.user.dao.UserInfoDao;
import com.farm.model.user.entity.UserInfo;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserInfoService {

	@Autowired
	private UserInfoDao userInfoDao;


	public List<UserInfo> getUserInfoBy(String  mobile,String openId){
		Map map = Maps.newHashMap();
		map.put("mobile",mobile);
		map.put("openId",openId);
		return  userInfoDao.getUserInfoBy(map);
	}


}
