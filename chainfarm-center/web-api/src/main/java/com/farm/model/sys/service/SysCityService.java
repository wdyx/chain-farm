package com.farm.model.sys.service;

import com.farm.model.sys.dao.SysClassDao;
import com.farm.model.sys.entity.SysClass;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SysCityService {

	@Autowired
	private SysClassDao sysClassDao;


	public List<SysClass> getSysClassBy(Integer  classSex,Integer classType){
		Map map = Maps.newHashMap();
		map.put("classSex",classSex);
		map.put("classType",classType);
		return  sysClassDao.getSysClassBy(map);
	}


}
