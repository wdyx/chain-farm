package com.farm.model.product.entity;

import com.farm.model.comm.CommEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品
 */
@Data
public class Product extends CommEntity {

    private Integer productId;

    private String productUrl;

    private String productName;

    private Integer productClassId;

    private String productNo;

    private Integer deleted;

    private Integer orderNum;

    private String picUrl;

    private String reservePrice;

    private String zkFinalPrice;

    private Integer volume;

}
