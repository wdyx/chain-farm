package com.farm.model.product.dto;

import com.taobao.api.domain.NTbkItem;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.internal.mapping.ApiListField;
import lombok.Data;

import java.util.List;

/**
 * 商品类
 */
@Data
public class TaoBaoResponse{

    private static final long serialVersionUID = 1199163469972573591L;

    /**
     * 淘宝客商品
     */
    @ApiListField("results")
    @ApiField("n_tbk_item")
    private List<NTbkItem> results;

    /**
     * 搜索到符合条件的结果总数
     */
    @ApiField("total_results")
    private Long totalResults;


}
