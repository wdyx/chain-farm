package com.farm.model.message.entity;

import com.farm.model.comm.CommEntity;
import lombok.Data;

/**
 * 消息内容
 */
@Data
public class MessageInfo extends CommEntity {

    private Integer msgId;

    private Integer userId;

    private Integer classId;

    private Integer cityId;

    private String content;

    private String telephone;

    private String address;

    private String remark;

    private Integer msgOrderNum;

}
