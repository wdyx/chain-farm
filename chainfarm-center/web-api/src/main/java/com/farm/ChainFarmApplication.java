package com.farm;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.thymeleaf.TemplateEngine;

@SpringBootApplication
@MapperScan("com.farm.model.*.dao")
public interface ChainFarmApplication {

    public static void main(String[] args) {

        //  使用Thymeleaf的layout:fragment
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.addDialect(new LayoutDialect());

        SpringApplication.run(ChainFarmApplication.class, args);
    }


}
