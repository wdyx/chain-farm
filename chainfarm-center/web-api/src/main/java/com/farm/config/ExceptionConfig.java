package com.farm.config;


import com.farm.config.exception.Result;
import com.farm.config.exception.ResultGenerator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 异常处理类
 */
@RestControllerAdvice
public class ExceptionConfig {

    @ExceptionHandler(Exception.class)
    public Result jsonErrorHandler(HttpServletRequest req, Exception e){
        e.printStackTrace();
        return ResultGenerator.genFailResult(e);
    }

}


