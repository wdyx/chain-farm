package com.farm.config.exception;

import lombok.Data;

/**
 * 响应类
 */
@Data
public class PageResult extends Result{

    /**
    * 总条数
    */
    private long  totalSize;

}
