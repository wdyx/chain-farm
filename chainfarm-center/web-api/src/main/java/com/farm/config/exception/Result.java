package com.farm.config.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 响应类
 */
@Data
public class Result {

    /**
     * 状态响应码
     */
    private int code;


    /**
     * 响应信息
     */
    private String msg;

    /**
     * 响应数据
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;

    public Result(int code, String msg,Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result() {
    }

}
