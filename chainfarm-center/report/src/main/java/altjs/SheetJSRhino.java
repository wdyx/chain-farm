package altjs;/* xlsx.js (C) 2013-present  SheetJS -- http://sheetjs.com */
/* vim: set ts=2: */

import altjs.com.sheetjs.SheetJS;
import altjs.com.sheetjs.SheetJSFile;
import altjs.com.sheetjs.SheetJSSheet;

public class SheetJSRhino {
  public static void main(String args[]) throws Exception {
    try {
      SheetJS sjs = new SheetJS();

      String file = "C:\\Users\\Administrator\\Desktop\\report\\test.xls";
      /* open file */
      SheetJSFile xl = sjs.read_file(file);

      /* get sheetnames */
      String[] sheetnames = xl.get_sheet_names();
      System.err.println(sheetnames[0]);



      /* convert to CSV */
      SheetJSSheet sheet = xl.get_sheet(0);
      String csv = sheet.get_csv();
      String range = sheet.get_range();
      System.out.println(csv);

    } catch(Exception e) {
      throw e;
    } finally {
      SheetJS.close();
    }
  }
}
