package com;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 */

@EnableScheduling
@SpringBootApplication
@Order(value = 1)
@Slf4j
@EntityScan("com.schedule.domain")
public class TestApplication {

    public static void main(String[] args) {

//        SpringApplication.run(TestApplication.class, args);
        ApplicationContext applicationContext = SpringApplication.run(TestApplication.class, args);
        SpringBeanLoader.setApplicationContext(applicationContext);
    }
}
