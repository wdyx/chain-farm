package com.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 工具类：数值转换
 */
public class NumConvertUtil {

    /**
     * double小数点后保存两位
     * @param d
     * @return
     */
    public static double formatDouble2(double d) {
        BigDecimal bg = new BigDecimal(d).setScale(2, RoundingMode.UP);
        return bg.doubleValue();
    }

    public static void main(String[] args) {
        System.out.println(formatDouble2(99.2387));
        System.out.println(formatDouble2(99.111));
    }

}
