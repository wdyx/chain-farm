package com.util;

import com.Constants;
import org.apache.commons.lang3.StringUtils;

/**
 * 工具类：时间表达式
 */
public class CronUtil {

    /**
     * 时间表达式经典案例(在线表达式：http://www.pppet.net/)
     * "30 * * * * ?" 每半分钟触发任务
     * "30 10 * * * ?" 每小时的10分30秒触发任务
     * "30 10 1 * * ?" 每天1点10分30秒触发任务
     * "30 10 1 20 * ?" 每月20号1点10分30秒触发任务
     * "30 10 1 20 10 ? *" 每年10月20号1点10分30秒触发任务
     * "30 10 1 20 10 ? 2011" 2011年10月20号1点10分30秒触发任务
     * "30 10 1 ? 10 * 2011" 2011年10月每天1点10分30秒触发任务
     * "30 10 1 ? 10 SUN 2011" 2011年10月每周日1点10分30秒触发任务
     * "15,30,45 * * * * ?" 每15秒，30秒，45秒时触发任务
     * "15-45 * * * * ?" 15到45秒内，每秒都触发任务
     * "15/5 * * * * ?" 每分钟的每15秒开始触发，每隔5秒触发一次
     * "15-30/5 * * * * ?" 每分钟的15秒到30秒之间开始触发，每隔5秒触发一次
     * "0 0/3 * * * ?" 每小时的第0分0秒开始，每三分钟触发一次
     * "0 15 10 ? * MON-FRI" 星期一到星期五的10点15分0秒触发任务
     * "0 15 10 L * ?" 每个月最后一天的10点15分0秒触发任务
     * "0 15 10 LW * ?" 每个月最后一个工作日的10点15分0秒触发任务
     * "0 15 10 ? * 5L" 每个月最后一个星期四的10点15分0秒触发任务
     * "0 15 10 ? * 5#3" 每个月第三周的星期四的10点15分0秒触发任务
     */

    /**
     * 将采集频率转换为时间表达式
     *  支持的频率：
     *      分钟：1-59（大于0，小于60）
     *      小时：hour
     *      天：day
     *      周：week
     *      月：month
     *      季度：quarter
     *      年：year
     * @param checkFrequency
     * @return
     */
    public static String getCornExpression(String checkFrequency) {
        String cornExpression = "";
        boolean flag = false;//为true时，使用生产环境采集周期，为false时，使用测试环境采集周期
        if(StringUtils.isNotEmpty(checkFrequency)){
            try{
                int a = Integer.valueOf(checkFrequency);
                if(Integer.valueOf(checkFrequency) > 0 && Integer.valueOf(checkFrequency) < 60){
                    cornExpression = "0 /" + checkFrequency + " * * * ?";//minutes分钟执行一次
                }else{
                    cornExpression = "非法时间表达式";
                }
            }catch (Exception e){
                if(flag){
                    if(Constants.CHECK_FREQUENCY_HOUR.equals(checkFrequency)){
                        cornExpression = "0 0 /1 * * ?";//每小时一次
                    }else if(Constants.CHECK_FREQUENCY_DAY.equals(checkFrequency)){
                        cornExpression = "0 10 0 * * ?";//每天凌晨零点10分
                    }else if(Constants.CHECK_FREQUENCY_WEEK.equals(checkFrequency)){
                        cornExpression = "0 10 1 ? * MON *";//每周一凌晨1点10分
                    }else if(Constants.CHECK_FREQUENCY_MONTH.equals(checkFrequency)){
                        cornExpression = "0 10 2 1 * ? *";//每月1号凌晨2点10分
                    }else if(Constants.CHECK_FREQUENCY_QUARTER.equals(checkFrequency)){
                        cornExpression = "0 10 3 1 1,4,7,11 ? *";//每季度（1月、4月、7月、11月）1号凌晨3点10分
                    }else if(Constants.CHECK_FREQUENCY_YEAR.equals(checkFrequency)){
                        cornExpression = "0 10 4 1 1 ? *";//每年1月1日凌晨4点10分
                    }
                }else{
                    //测试使用
                    if(Constants.CHECK_FREQUENCY_HOUR.equals(checkFrequency)){
                        cornExpression = "0 /10 * * * ?";//每10分钟执行一次
                    }else if(Constants.CHECK_FREQUENCY_DAY.equals(checkFrequency)){
                        cornExpression = "0 /20 * * * ?";//每20分钟执行一次
                    }else if(Constants.CHECK_FREQUENCY_WEEK.equals(checkFrequency)){
                        cornExpression = "0 /30 * * * ?";//每30分钟执行一次
                    }else if(Constants.CHECK_FREQUENCY_MONTH.equals(checkFrequency)){
                        cornExpression = "0 /40 * * * ?";//每40分钟执行一次
                    }else if(Constants.CHECK_FREQUENCY_QUARTER.equals(checkFrequency)){
                        cornExpression = "0 /50 * * * ?";//每50分钟执行一次
                    }else if(Constants.CHECK_FREQUENCY_YEAR.equals(checkFrequency)){
                        cornExpression = "0 /59 * * * ?";//每1小时执行一次
                    }
                }
            }
        }
        return cornExpression;
    }

    /**
     * 获取时间表达式，传入分钟，返回指定每隔该分钟数执行一次
     * @param minutes
     * @return
     */
    public static String getCornExpressionMinutes(String minutes) {
        String cornExpression = "";
        if(StringUtils.isNotEmpty(minutes)){
            if(Integer.valueOf(minutes) > 0 && Integer.valueOf(minutes) < 60){
                cornExpression = "0 /" + minutes + " * * * ?";//minutes分钟执行一次
            }
        }
        return cornExpression;
    }

    /**
     * 获取时间表达式，传入秒数，返回指定每隔该秒数执行一次
     * @param seconds
     * @return
     */
    public static String getCornExpressionSeconds(String seconds) {
        String cornExpression = "";
        if(StringUtils.isNotEmpty(seconds)){
            if(Integer.valueOf(seconds) > 0 && Integer.valueOf(seconds) < 60){
                cornExpression = "0/" + seconds + " * * * * ?";//seconds秒执行一次
            }
        }
        return cornExpression;
    }

    public static void main(String[] args) {
        System.out.println(getCornExpression("515"));
    }

}
