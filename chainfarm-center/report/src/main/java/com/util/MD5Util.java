package com.util;
/**
* @desc MD5加密
* @author  wangfeng
* @date  2018/7/11 16:41
**/
public class MD5Util {

	public static String getMD5(byte[] source) {
		String s = null;
		char hexDigits[] = { // 用来将字节转换成 16 进制表示的字�?
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			md.update(source);
			byte tmp[] = md.digest(); // MD5 的计算结果是�?�� 128 位的长整数，
										// 用字节表示就�?16 个字�?
			char str[] = new char[16 * 2]; // 每个字节�?16 进制表示的话，使用两个字符，
											// �?��表示�?16 进制�?�� 32 个字�?
			int k = 0; // 表示转换结果中对应的字符位置
			for (int i = 0; i < 16; i++) { // 从第�?��字节�?��，对 MD5 的每�?��字节
											// 转换�?16 进制字符的转�?
				byte byte0 = tmp[i]; // 取第 i 个字�?
				str[k++] = hexDigits[byte0 >>> 4 & 0xf]; // 取字节中�?4 位的数字转换,
															// >>>
															// 为�?辑右移，将符号位�?��右移
				str[k++] = hexDigits[byte0 & 0xf]; // 取字节中�?4 位的数字转换
			}
			s = new String(str); // 换后的结果转换为字符�?

		} catch (Exception e) {
			e.printStackTrace();
		}
		return s.toUpperCase();
	}

	/**
	* @desc 获取MD5加密后的前8位
	* @param
	* @author  wangfeng
	* @date  2018/7/11 16:45
	* @return
	**/
	public static String getMD58(byte[] source){
		String md5Str = MD5Util.getMD5(source);
		return md5Str.substring(0,8);
	}

	/**
	 *@Author: joke
	 *@Date: 17:43 2018/8/13
	 *@Description: 根据kpiName,获取kpiId
	*/
	public static String makeKpiId(String kpiName){
		String kpiId= "KPI"+MD5Util.getMD58(kpiName.getBytes());
		return kpiId;
	}

	public static void main(String[] args){
		String[] name={"海虹列头柜频率","台达列头柜频率","精密空调回风湿度"
				,"精密空调回风温度","精密空调风机状态","精密空调风机电流"
				,"精密空调电机电流","精密空调压缩机电流","精密空调加湿器状态"
				,"精密空调电加热状态"};
		for (int i = 0; i <name.length ; i++) {
			String uuid= "KPI"+MD5Util.getMD58(name[i].getBytes());
//			System.out.println(name[i]+"-----:"+uuid);
			System.out.println(uuid);
		}
//		for (int i = 1; i <7 ; i++) {
//			String name1="输入"+i+"计数";
//			String uuid= "KPI"+MD5Util.getMD58(name1.getBytes());
//			System.out.println(uuid);
//		}

	}

}
