package com.util;

import com.Constants;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 公共工具类
 */
public class Utils {

    /**
     * 判断是否是表达式
     *
     * @param str
     * @return
     */
    public static boolean isExp(String str) {
        if (StringUtils.isNotBlank(str)) {
            if (str.startsWith(Constants.SQL) ||
                    str.startsWith(Constants.RESID) ||
                    str.startsWith(Constants.RESTYPE) ||
                    str.startsWith(Constants.KPI) ||
                    str.startsWith(Constants.FIELD) ||
                    str.startsWith(Constants.INPUT) ||
                    str.startsWith(Constants.SELECT) ||
                    str.startsWith(Constants.LOOP) ||
                    str.startsWith(Constants.COL)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否是表达式
     *
     * @param str
     * @return
     */
    public static boolean isExp(String str, String preFix) {
        if (StringUtils.isNotBlank(str) && str.startsWith(preFix)) {
            return true;
        }
        return false;
    }


    /**
     * 获取输入文件流
     *
     * @param inputFilePath
     * @return
     * @throws FileNotFoundException
     */
    public static InputStream getInFileStream(String inputFilePath) throws FileNotFoundException {
        File inFile = new File(inputFilePath);
        FileInputStream iStream = new FileInputStream(inFile);
        return iStream;
    }

    /**
     * 获取输出文件流
     *
     * @param outputFilePath
     * @return
     * @throws IOException
     */
    public static OutputStream getOutFileStream(String outputFilePath) throws IOException {
        File outFile = new File(outputFilePath);

        try {
            outFile.getParentFile().mkdirs();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        outFile.createNewFile();
        FileOutputStream oStream = new FileOutputStream(outFile);
        return oStream;
    }


    /**
     * 获取字符串拼音的第一个字母
     * @param chinese
     * @return
     */
    public static String ToFirstChar(String chinese){
        String pinyinStr = "";
        char[] newChar = chinese.toCharArray();  //转为单个字符
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < newChar.length; i++) {
            if (newChar[i] > 128) {
                try {
                    pinyinStr += PinyinHelper.toHanyuPinyinStringArray(newChar[i], defaultFormat)[0].charAt(0);
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            }else{
                pinyinStr += newChar[i];
            }
        }
        return pinyinStr;
    }

    /**
     * 汉字转为拼音
     * @param chinese
     * @return
     */
    public static String ToPinyin(String chinese){
        String pinyinStr = "";
        char[] newChar = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < newChar.length; i++) {
            if (newChar[i] > 128) {
                try {
                    pinyinStr += PinyinHelper.toHanyuPinyinStringArray(newChar[i], defaultFormat)[0];
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            }else{
                pinyinStr += newChar[i];
            }
        }
        return pinyinStr;
    }


    public static String formatFileName(String name) {
        Date now = new Date();
        return name.replaceAll("\\{yyyy}", new SimpleDateFormat("yyyy").format(now))
                .replaceAll("\\{MM}", new SimpleDateFormat("MM").format(now))
                .replaceAll("\\{dd}", new SimpleDateFormat("dd").format(now))
                .replaceAll("\\{HH}", new SimpleDateFormat("HH").format(now))
                .replaceAll("\\{mm}", new SimpleDateFormat("mm").format(now))
                .replaceAll("\\{ss}", new SimpleDateFormat("ss").format(now))
                ;
    }

    /**
     * 获取报表生成路径
     * @param path 报表生成基础路径
     * @param name 报表名称
     * @param suffix 后缀
     * @return
     */
    public static String getReportPath(String path,String name,String suffix){
        Calendar calendar = Calendar.getInstance();
        // 获取年份，月份，日
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        // 报表名称
        String reportName = formatFileName(name) + suffix;
        return path + File.separator + year + File.separator + month + File.separator + day + File.separator + reportName;
    }

    /**
     * 转换为int
     * @param num
     * @return
     */
    public static int convertInt(String num){
        if(StringUtils.isNotBlank(num)){
            return Integer.parseInt(num);
        }
        return -1;
    }


    public static void main(String[] args) {
        System.out.println(ToFirstChar("4201-软件系统数据库日运行监控表-模板")); //转为首字母大写
        System.out.println(ToPinyin("汉字转换为拼音"));
    }
}
