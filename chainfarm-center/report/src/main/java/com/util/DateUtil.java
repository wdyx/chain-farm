package com.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @ClassName: DateUtil
 * @Description: 时间工具类
 * @Author: 赵鹏军
 * @Date: 2018/10/18 15:58
 * @Version 1.0
 */
public class DateUtil {

    /**
     * @Description 获取当前时间
     * @return java.lang.String 格式：yyyy-MM-dd HH:mm:ss
     * @Date 2018/10/20 17:06
     * @Author 赵鹏军
     **/
    public static String now() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        return simpleDateFormat.format(new Date());
    }

}
