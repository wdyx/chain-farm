package com.util.exportout.pdf;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-8-8
 * Time: 下午1:29
 * To change this template use File | Settings | File Templates.
 */
public class PdfFontManager {
    private HashMap<String, Font> pdfFonts = new HashMap<String, Font>();
    public void createFontForInfo(){
        HashMap<String,Font> resultFontList = new HashMap<String, Font>();
        try {
            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",  BaseFont.NOT_EMBEDDED);
            resultFontList.put("8FOLD",new Font(bfChinese, 8, Font.BOLD));
            resultFontList.put("8NORMAL",new Font(bfChinese, 8, Font.NORMAL));
        } catch (Exception e) {
            e.printStackTrace();
        }
        pdfFonts.putAll(resultFontList);
    }

    public HashMap<String, Font> getBaseFonts(){
        this.createFontForInfo();
        return pdfFonts;
    }



}
