package com.util.exportout.GenerateFileUtil;

import java.io.UnsupportedEncodingException;

public class FileNameUtil {

    static String fileNameRegex = "[/\\\\<>*?|\"]";

    public static String fileNameReplaceUnLimitSymble(String fileName){

        String retFileName =  fileName.replaceAll(fileNameRegex, "");
        try {
            retFileName = new String(retFileName.getBytes(), "iso8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return retFileName;
    }


}
