package com.util.exportout.csv;


import com.lowagie.text.DocumentException;
import com.util.exportout.FileManager;
import com.util.exportout.IFileData;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class CsvManager extends FileManager implements IFileData {
    protected int startLine;//开始的行
    protected List<String> heads = new ArrayList<String>();//记录头部数据
    protected String contentStr = "";//输出内容
    protected String fileName = "csv";

    public CsvManager(){

    }

    public CsvManager(String fName){
        this.setFileName(fName);
    }

    @Override
    public void init(String fileName) {
        this.setFileName(fileName);
    }

    /**
     * 按照字符串，创建Csv头部文件
     * @param dataInfo csv头部信息
     * @return 写入头信息后最后一行的位置
     */
    @Override
    public int creatDataInfoByStr(List<String> dataInfo)  throws DocumentException {
        for(String infoItem : dataInfo){
            startLine ++;
            contentStr = contentStr + infoItem.replace("&",",") + "\r\n";
        }
        return startLine;
    }

    /**
     * 创建头部数据
     * @param dataHead 头部数据
     * @return 写入最后一行的位置
     */
    @Override
    public int creatDataHeadByListStr(List<String> dataHead)  throws DocumentException{
        //TODO &amp 替换为别的特殊字符
        for(String headItem:dataHead){
            String[] keys = headItem.split("&");
            heads.add(keys[0]);//把列名放到集合中
            if(keys.length>=2){//第二项存在
                contentStr = contentStr + keys[1] + ",";
            }
            startLine++;
        }
        //TODO替换为&
        contentStr =  contentStr.substring(0,contentStr.length()-1) + "\r\n";
        return startLine;
    }
    /**
     * 根据查询HashMap类型的结果，输出
     * @param dataBody HashMap查询的结果
     * @return 写入信息后最后一行的位置
     */
    @Override
    public int creatDataBodyByListMap(List<HashMap<String,String>> dataBody)  throws DocumentException{
        for(HashMap<String,String> bodyItem : dataBody){
            String dataLine = "";
            for(String key:heads){//根据头部进行输出，而不是身体数据
                dataLine = dataLine + bodyItem.get(key).toString()+ ",";
            }
            dataLine = dataLine.substring(0,dataLine.length()-1);
            contentStr = contentStr + dataLine + "\r\n";
            startLine ++;
        }
        return startLine;
    }

    /**
     * 输出文件
     * @return fullName     文件路径+文件名称
     */
    @Override
    public String outToFile()  throws DocumentException{
        String fullName = getFileDir() + this.getFileName() + ".csv";
        try{
            FileOutputStream fos=new FileOutputStream(fullName);
            OutputStreamWriter osw= new OutputStreamWriter(fos);
            BufferedWriter bw=new BufferedWriter(osw);
            bw.write(contentStr);
            bw.close();
            osw.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return fullName;
    }

    public int getStartLine() {
        return startLine;
    }

    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
