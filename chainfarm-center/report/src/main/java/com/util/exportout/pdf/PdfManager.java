package com.util.exportout.pdf;


import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.util.exportout.FileManager;
import com.util.exportout.IFileData;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class PdfManager extends FileManager implements IFileData {

    protected Document document;
    protected String fileName = "pdf";
    protected PdfTableManager pdfTableManager = new PdfTableManager();
    protected PdfCellManager  pdfCellManager  = new PdfCellManager();
    protected FileOutputStream fileOut;
    protected List<String> heads = new ArrayList<String>();//记录头部数据
    protected int pdfMaxWidth;
    public PdfManager(){

    }

    public PdfManager(String fName){
        this.setFileName(fName);
        this.setPdfMaxWidth(520);
        document  = new Document();
        document.setPageSize(PageSize.A4);// 设置页面大小
        try {
            File file = new File(getFileDir() + this.getFileName() + ".pdf");
            file.createNewFile();
            fileOut =new FileOutputStream(file);
            PdfWriter.getInstance(document,fileOut );
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(String fileName) {
        //设置文件名称
        this.setFileName(fileName);
        this.setPdfMaxWidth(520);
        document  = new Document();
        document.setPageSize(PageSize.A4);// 设置页面大小
        try {
            File file = new File(getFileDir() + this.getFileName() + ".pdf");
            file.createNewFile();
            fileOut =new FileOutputStream(file);
            PdfWriter.getInstance(document,fileOut );
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int creatDataInfoByStr(List<String> dataInfo) throws DocumentException {
        int maxLength = 0;
        //遍历数组，获得最大的长度
        for (String infoItem:dataInfo){
            String items[] = infoItem.split("&");
            if(items.length > maxLength){
                maxLength = items.length;
            }
        }
        PdfPTable table = pdfTableManager.createTableForInfo(maxLength,pdfMaxWidth);//创建一行数据
        //写入数据
        for (String infoItem:dataInfo){
            String items[] = infoItem.split("&");
            for(int i=0;i<items.length;i++){
                if(items.length < maxLength){//小于总长度，要进行合并
                    if(i == items.length-1){//最后一个元素进行合并
                        table.addCell(pdfCellManager.createCellForInfo(items[i],maxLength-items.length+1));
                    }else{
                        table.addCell(pdfCellManager.createCellForInfo(items[i]));
                    }
                }else{
                    table.addCell(pdfCellManager.createCellForInfo(items[i]));
                }
            }
        }
        document.add(table);
        return 0;
    }

    @Override
    public int creatDataHeadByListStr(List<String> dataHead) throws DocumentException {
        PdfPTable table = pdfTableManager.createTableForInfo(dataHead.size(),pdfMaxWidth);//创建一行数据
        for(String headItem:dataHead){
            String[] keys = headItem.split("&");
            heads.add(keys[0]);//把列名放到集合中
            if(keys.length>=2){//第二项存在
                table.addCell(pdfCellManager.createCellCenterBlue(keys[1]));
            }
        }
        document.add(table);
        return 0;
    }

    @Override
    public int creatDataBodyByListMap(List<HashMap<String, String>> dataBody)throws DocumentException  {
        PdfPTable table = pdfTableManager.createTableForInfo(heads.size(), pdfMaxWidth);//创建一行数据
        for(int i=0;i<dataBody.size();i++) {
            for(int j=0;j<heads.size();j++) {
                table.addCell(pdfCellManager.createCellCenterWrite((dataBody.get(i).get(heads.get(j))).toString()));
            }
        }
        document.add(table);
        return 0;
    }

    @Override
    public String outToFile() throws DocumentException{
        document.close();
        try{
            fileOut.flush();
            fileOut.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return getFileDir() + this.getFileName() + ".pdf";
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getPdfMaxWidth() {
        return pdfMaxWidth;
    }

    public void setPdfMaxWidth(int pdfMaxWidth) {
        this.pdfMaxWidth = pdfMaxWidth;
    }
}
