package com.util.exportout.excel;

import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class ExcelUtil {
    /**
     * 格式化单元格内容
     *
     * @param cell
     * @param dictInfo 转义
     * @return
     */
    public static String getCellFormatValue(Cell cell, HashMap<String, String> dictInfo) {
        String cellvalue = ExcelUtil.getCellFormatValue(cell);
        if (dictInfo.containsKey(cellvalue)) {
            cellvalue = dictInfo.get(cellvalue);
        }
        return cellvalue;
    }

    /**
     * 格式化单元格内容
     *
     * @param cellvalue
     * @param dictInfo  转义
     * @return
     */
    public static String getCellFormatValue(String cellvalue, HashMap<String, String> dictInfo) {
        if (dictInfo.containsKey(cellvalue)) {
            cellvalue = dictInfo.get(cellvalue);
        }
        return cellvalue;
    }

    /**
     * 格式化单元格内容
     *
     * @param cell
     * @return
     */
    public static String getCellFormatValue(Cell cell) {
        String cellvalue = "";
        if (cell != null) {
            // 判断当前Cell的Type
//            switch (cell.getCellType()) {
//
//                // 如果当前Cell的Type为NUMERIC
//                case Cell .NUMERIC:
//                case Cell.CELL_TYPE_FORMULA: {
//                    if (DateUtil.isCellDateFormatted(cell)) {
//                        cellvalue = cell.getDateCellValue().toString();
//                    }
//                    else {
//                        int turnInt = (int)cell.getNumericCellValue();
//                        cellvalue = String.valueOf(turnInt);
//                    }
//                    break;
//                }
//                case Cell.CELL_TYPE_STRING:
//                    cellvalue = cell.getRichStringCellValue().getString();
//                    if(cellvalue.matches("[0-9](\\.?[0-9])?")){
//                        double in = Double.parseDouble(cellvalue);
//                        int turnInt = (int)in;
//                        cellvalue = String.valueOf(turnInt);
//                    }
//                    break;
//                default:
//                    cellvalue = " ";
//            }

            switch (cell.getCellTypeEnum().name()) {
                case "NUMERIC": // 数字
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellvalue = cell.getDateCellValue().toString();
                    } else {
                        int turnInt = (int) cell.getNumericCellValue();
                        cellvalue = String.valueOf(turnInt);
                    }
                    break;
                case "STRING": // 字符串
                    cellvalue = cell.getRichStringCellValue().getString();
                    if (cellvalue.matches("[0-9](\\.?[0-9])?")) {
                        double in = Double.parseDouble(cellvalue);
                        int turnInt = (int) in;
                        cellvalue = String.valueOf(turnInt);
                    }
                    break;
                case "FORMULA":
                    cellvalue = cell.getCellFormula();
                    break;
                default:
                    cellvalue = "";
                    break;
            }
        } else {
            cellvalue = "";
        }
        return cellvalue.trim();
    }

    /**
     * 获得全部字典值
     *
     * @param wb
     * @param lastSheetNum 最后几个
     * @param keyCols
     * @param valueCols
     * @return
     */
    public static HashMap<String, String> dictInfo(Workbook wb, int lastSheetNum, int keyCols, int valueCols) throws Exception {
        return ExcelUtil.dictInfo(wb, wb.getNumberOfSheets() - lastSheetNum, wb.getNumberOfSheets() - 1, keyCols, valueCols);
    }

    /**
     * 获得全部字典值
     *
     * @param wb
     * @param startSheetNum 开始的sheet
     * @param endSheetNum   结束的sheet
     * @param keyCols       key列
     * @param valueCols     value列
     * @return
     */
    public static HashMap<String, String> dictInfo(Workbook wb, int startSheetNum, int endSheetNum, int keyCols, int valueCols) throws Exception {
        HashMap<String, String> result = new HashMap<String, String>();
        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            Sheet sheet = wb.getSheetAt(i);
            String sheetName = sheet.getSheetName();
            if (sheetName.indexOf("DICT") == -1) {
                continue;
            }
            int colNum = ExcelUtil.colDataNum(wb, i);
            int rowNum = ExcelUtil.rowDataNum(wb, i, colNum);
            for (int j = 1; j < rowNum; j++) {//标题不要
                Row row = sheet.getRow(j);
                result.put(ExcelUtil.getCellFormatValue(row.getCell(keyCols)), ExcelUtil.getCellFormatValue(row.getCell(valueCols)));
            }
        }
        return result;
    }

    /**
     * 获得上线资源路径
     *
     * @return
     */
    public static String getResourcePathOnline() {
        return ExcelUtil.getResourcePath() + "onLine" + File.separator;
    }

    /**
     * 获得cccp资源路径
     *
     * @return
     */
    public static String getResourcePath() {
        return "";
    }

    /**
     * 复制一个excel到指定目录下
     *
     * @param fileName
     * @return
     * @throws Exception
     */
    public static String copyExcel(String fileName) throws Exception {
        String tmpPath = ExcelUtil.getResourcePathOnline();
        String sourceFile = tmpPath + fileName;
        String[] fileNameArr = fileName.split("[.]");
        String targetFile = tmpPath + fileNameArr[0] + "_copy." + fileNameArr[1];
        InputStream is = new FileInputStream(new File(sourceFile));
        OutputStream os = new FileOutputStream(new File(targetFile));
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
        is.close();
        os.flush();
        os.close();
        return targetFile;
    }

    /**
     * @param wb
     * @param sheetNum      清空sheet页序号
     * @param cleanRowStart 清空开始的行号 -1表示从0开始
     * @param cleanRowEnd   清空结束的行号 -1表示直到数据结尾
     * @param cleanColStart 清空开始的列号 -1表示从0开始
     * @param cleanColEnd   清空结束的列号 -1表示直到数据结尾
     * @return
     * @throws Exception
     */
    public static Workbook cleanExcel(Workbook wb, int sheetNum, int cleanRowStart, int cleanRowEnd, int cleanColStart, int cleanColEnd) throws Exception {
        Sheet sheet = wb.getSheetAt(sheetNum);
        int colNum = ExcelUtil.colDataNum(wb, sheetNum);
        int rowNum = ExcelUtil.rowDataNum(wb, sheetNum, colNum);
        for (int j = 0; j < rowNum; j++) {
            if ((cleanRowStart <= j || cleanRowStart == -1) && (j <= cleanRowEnd || cleanRowEnd == -1)) {
                Row row = sheet.getRow(j);
                for (int k = 0; k < colNum; k++) {
                    if ((cleanColStart <= k || cleanColStart == -1) && (k <= cleanColEnd || cleanColEnd == -1)) {
                        if (row.getCell(k) != null) {
                            row.getCell(k).setCellValue("");
                        }
                    }
                }
            }
        }
        return wb;
    }

    /**
     * 获得sheet标记是第几个sheet
     *
     * @param wb
     * @param sheetFlag
     * @return
     */
    public static int findSheetNumbySheetFlag(Workbook wb, String sheetFlag) throws Exception {
        int result = -1;
        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            if (wb.getSheetName(i).indexOf(sheetFlag) != -1) {
                result = i;
            }
        }
        return result;
    }

    /**
     * 获取一个sheet页的关键字在第几列
     *
     * @param wb
     * @param sheetNum
     * @return
     */
    public static HashMap<String, Integer> findKeyColNum(Workbook wb, int sheetNum) throws Exception {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        Sheet sheet = wb.getSheetAt(sheetNum);
        //获得标题总列数
        Row rowColTotal = sheet.getRow(0);
        int colNum = ExcelUtil.colDataNum(wb, sheetNum);
        for (int k = 0; k < colNum; k++) {
            String collNameContent = rowColTotal.getCell(k).getStringCellValue();
            String collRealName = collNameContent.substring(collNameContent.indexOf("(") + 1, collNameContent.indexOf(")"));
            result.put(collRealName, k);
        }
        return result;
    }

    /**
     * @param wb
     * @param sheetNum
     * @return
     * @throws Exception
     */
    public static int colDataNum(Workbook wb, int sheetNum) throws Exception {
        return ExcelUtil.colDataNum(wb, sheetNum, 0);
    }

    /**
     * 根据第几行判断总列数
     *
     * @param wb
     * @param sheetNum
     * @param rowNum
     * @return
     * @throws Exception
     */
    public static int colDataNum(Workbook wb, int sheetNum, int rowNum) throws Exception {
        int i = 0;
        Sheet sheet = wb.getSheetAt(sheetNum);
        Row row = sheet.getRow(rowNum);
        while (true) {
            if (("").equals(getCellFormatValue(row.getCell(i)))) {//如果值为空，就认为结束了
                break;
            } else {
                i++;
            }
        }
        return i;
    }

    /**
     * 返回到第几行完全没有数据(根据列数判断)
     *
     * @param wb
     * @param sheetNum 第几个sheet
     * @param colNum   列的个数
     * @return 最后一行所在行数 ，实际数据为cell（n-1）
     * @throws Exception
     */
    public static int rowDataNum(Workbook wb, int sheetNum, int colNum) throws Exception {
        int i = 0;
        Sheet sheet = wb.getSheetAt(sheetNum);
        while (true) {
            int emptyNum = colNum;
            Row row = sheet.getRow(i);
            if (row == null) {//行为空直接跳出
                break;
            }
            for (int j = 0; j < colNum; j++) {
                if (("").equals(getCellFormatValue(row.getCell(j)))) {
                    emptyNum--;
                }
            }
            if (emptyNum == 0) {
                break;
            } else {
                i++;
            }
        }
        return i;
    }

}
