package com.util.exportout.html;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-10-10
 * Time: 下午1:25
 * To change this template use File | Settings | File Templates.
 */
public class HtmlCssManager {
    String baseStyleContent = "";
    public String infoStyleForBase(){
        String infoStyle = ".infoStyle{" + "\r\n" +
                "width: 100%;" + "\r\n" +
                "color: #003399;" + "\r\n" +
                "font-size: 12px;" + "\r\n" +
                "border:1px solid #ccc;" + "\r\n" +
                "border-collapse: collapse;" + "\r\n" +
                "margin: 0;" + "\r\n" +
                "padding: 0;" + "\r\n" +
                "}";
        infoStyle = infoStyle + ".infoStyle td{" + "\r\n" +
                "    background-color: #EDF1F5;" + "\r\n" +
                "    border: 1px solid blue;" + "\r\n" +
                "    height: 18px;" + "\r\n" +
                "    padding: 3px;" + "\r\n" +
                "    text-align: left;" + "\r\n" +
                "}";
        return infoStyle;
    }
    public String mainStyleForBase(){
        String headStyle = ".mainStyle{" + "\r\n" +
                "margin: 10px 0 0;" + "\r\n" +
                "width: 100%;" + "\r\n" +
                "color: #003399;" + "\r\n" +
                "font-size: 12px;" + "\r\n" +
                "border:1px solid #ccc;" + "\r\n" +
                "border-collapse: collapse;" + "\r\n" +
                "padding: 0;" + "\r\n" +
                "}";
        headStyle = headStyle + ".mainStyle thead td{" + "\r\n" +
                "    background-color: yellow;" + "\r\n" +
                "    border: 1px solid blue;" + "\r\n" +
                "    height: 18px;" + "\r\n" +
                "    padding: 3px;" + "\r\n" +
                "    text-align: left;" + "\r\n" +
                "}";
        headStyle = headStyle + ".mainStyle tbody td{" + "\r\n" +
                "    background-color: #ffffff;" + "\r\n" +
                "    border: 1px solid blue;" + "\r\n" +
                "    height: 18px;" + "\r\n" +
                "    padding: 3px;" + "\r\n" +
                "    text-align: left;" + "\r\n" +
                "}";
        //thead设置头
        //tbody设置内容
        return headStyle;
    }
    public String getBaseStyle(){
        baseStyleContent = baseStyleContent + "<style type=\"text/css\">" + "\r\n";
        baseStyleContent = baseStyleContent + this.infoStyleForBase()+this.mainStyleForBase();
        baseStyleContent = baseStyleContent + "</style >" + "\r\n";
        return baseStyleContent;
    }
}
