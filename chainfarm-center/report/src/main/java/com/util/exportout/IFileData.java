package com.util.exportout;

import com.lowagie.text.DocumentException;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-8-9
 * Time: 上午10:53
 * To change this template use File | Settings | File Templates.
 */
public interface IFileData {
    public void init(String fileName);
    public void setFileDir(String fileName);
    public int creatDataInfoByStr(List<String> dataInfo) throws DocumentException;
    public int creatDataHeadByListStr(List<String> dataHead) throws DocumentException;
    public int creatDataBodyByListMap(List<HashMap<String, String>> dataBody) throws DocumentException;
    public String outToFile() throws DocumentException;
}
