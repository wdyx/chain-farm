package com.util.exportout.excel.abstr;

import com.util.exportout.excel.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shl
 * Date: 14-2-21
 * Time: 下午1:31
 * To change this template use File | Settings | File Templates.
 */
public class ExcelReadManager {
    public List<HashMap<String, List<HashMap<String, String>>>> sheetContent(Workbook wb) throws Exception{
        List<HashMap<String, List<HashMap<String, String>>>> result = new ArrayList<HashMap<String, List<HashMap<String, String>>>>();
        HashMap<String,String> distInfo = ExcelUtil.dictInfo(wb, 4, 1, 0);
        for(int i = 0;i < wb.getNumberOfSheets();i++){//第一个sheet页为基本信息，最后一个为附属信息
            Sheet sheet = wb.getSheetAt(i);
            String sheetName = sheet.getSheetName();
            String nameArray[] = sheetName.split("-");
            if(nameArray.length != 3){
                continue;
            }
            int colNum = ExcelUtil.colDataNum(wb,i);
            int rowNum = ExcelUtil.rowDataNum(wb,i,colNum);
            List<String> colName = new ArrayList<String>();
            List<HashMap<String, String>>  resultList = new ArrayList<HashMap<String,String>>();
            HashMap<String, List<HashMap<String, String>>> resultHashMap = new HashMap<String, List<HashMap<String, String>>>();
            for (int j = 0; j < rowNum; j++) {
                Row row = sheet.getRow(j);
                HashMap<String,String> rowData = new HashMap<String, String>();
                for(int k = 0;k < colNum;k ++){
                    if(j==0){//第一行数据为标题
                        String collNameContent = ExcelUtil.getCellFormatValue(row.getCell(k));
                        String colrealName = collNameContent.substring(collNameContent.indexOf("(") + 1,collNameContent.indexOf(")"));
                        colName.add(colrealName);
                    }else{
                        String cellContent = "";
                        if(row!= null && row.getCell(k) != null){
                            cellContent = ExcelUtil.getCellFormatValue(row.getCell(k),distInfo);
                        }
                        rowData.put(colName.get(k),cellContent);
                    }
                }
                if(j != 0){
                    resultList.add(rowData);
                }
            }
            if(resultList.size() != 0){
                resultHashMap.put("TAB1",resultList);
                result.add(resultHashMap);
            }
        }
        return result;
    }

    /**
     * 获得sheet数据
     * @param wb
     * @return
     */
    public List<HashMap<String,String>> sheetTypeList(Workbook wb) throws Exception{
        List<HashMap<String,String>> result = new ArrayList<HashMap<String, String>>();
        for(int i = 0;i < wb.getNumberOfSheets();i++){//第一个sheet页为基本信息，最后一个为附属信息
            HashMap<String,String> hashMap = new HashMap<String, String>();
            Sheet sheet = wb.getSheetAt(i);
            String sheetName = sheet.getSheetName();
            if(sheetName.indexOf("BIZ") == -1){
                continue;
            }
            int colNum = ExcelUtil.colDataNum(wb,i);
            int rowNum = ExcelUtil.rowDataNum(wb,i,colNum);
            if(rowNum > 1){
                String[] sheetInfo= sheetName.split("-");
                hashMap.put("NODE_NAME",sheetInfo[2]);
                hashMap.put("NODE_TYPE",sheetInfo[1]);
                hashMap.put("NODE_STYLE","STYLE_" + sheetInfo[1]);
                result.add(hashMap);
            }
        }
        return result;
    }

}
