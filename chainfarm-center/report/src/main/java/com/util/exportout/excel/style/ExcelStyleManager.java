package com.util.exportout.excel.style;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-8-7
 * Time: 下午4:42
 * To change this template use File | Settings | File Templates.
 */
public class ExcelStyleManager {
    private  HashMap<String, CellStyle> styles = new HashMap<String, CellStyle>();

    //基础样式，返回有四个黑色边框，
    public void infoStyleForBase(Workbook wb){
        CellStyle style;
        //字体
        Font infoFont = wb.createFont();
        infoFont.setFontName("宋体");
        infoFont.setFontHeightInPoints((short)14);
        infoFont.setColor(IndexedColors.BLACK.getIndex());
        infoFont.setBold(true);

        style = wb.createCellStyle();
//        //边框
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        //位置
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(infoFont);
        styles.put("infoStyleBase", style);
    }

    public void titleStyleForBase(Workbook wb){
        CellStyle style;
        //字体
        Font infoFont = wb.createFont();
        infoFont.setFontName("宋体");
        infoFont.setFontHeightInPoints((short)16);
        infoFont.setColor(IndexedColors.BLACK.getIndex());
        infoFont.setBold(true);

        style = wb.createCellStyle();
//        //边框
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        //位置
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(infoFont);
        styles.put("titleStyleBase", style);
    }

    public void subTitleStyleForBase(Workbook wb){
        CellStyle style;
        //字体
        Font infoFont = wb.createFont();
        infoFont.setFontName("宋体");
        infoFont.setFontHeightInPoints((short)14);
        infoFont.setColor(IndexedColors.DARK_RED.getIndex());
        infoFont.setBold(true);

        style = wb.createCellStyle();
//        //边框
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        //位置
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(infoFont);
        styles.put("subTitleStyleBase", style);
    }

    public void headStyleForBase(Workbook wb){
        CellStyle style;
        style = wb.createCellStyle();

        //背景颜色
        style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
//        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//        //边框
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        //位置
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        //字体
        Font infoFont = wb.createFont();
        infoFont.setFontName("宋体");
        infoFont.setFontHeightInPoints((short)14);
        infoFont.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(infoFont);
        styles.put("headStyleBase", style);
    }
    public void bodyStyleForBase(Workbook wb){
        CellStyle style;
        //字体
        Font infoFont = wb.createFont();
        infoFont.setFontName("宋体");
        infoFont.setFontHeightInPoints((short)14);
        infoFont.setColor(IndexedColors.BLACK.getIndex());

        style = wb.createCellStyle();
//        //边框
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//
//        //位置
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(infoFont);

        styles.put("bodyStyleBase", style);
    }

    public HashMap<String, CellStyle> getBaseStyle(Workbook wb){
        this.infoStyleForBase(wb);
        this.headStyleForBase(wb);
        this.bodyStyleForBase(wb);
        this.titleStyleForBase(wb);
        this.subTitleStyleForBase(wb);
        return styles;
    }
}
