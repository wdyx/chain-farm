package com.util.exportout.excel.abstr;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.util.exportout.FileManager;
import com.util.exportout.IFileData;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-8-7
 * Time: 下午3:00
 * excel抽象父类，
 * TODO 实现文件接口
 */
public class ExcelManager extends FileManager implements IFileData {
    protected int startRows; //设置内容开始行
    protected int startCols; //设置内容开始列
    protected List<String> heads = new ArrayList<String>();//记录头部数据
    protected Workbook wb;//实例化的wb
    protected Sheet sheet;//操作的sheet
    protected String Suffix;//设置文件后缀
    protected HashMap<String, CellStyle> styles;//样式集合
    protected String fileName = "excel";
    public ExcelManager(){
    }
    public ExcelManager(String fName){
        this.setFileName(fName);
    }
    public Workbook creatWorkBook(){
        return null;
    }

    @Override
    public void init(String fileName) {
        this.setFileName(fileName);
    }

    /**
     * 创建头部信息，每一组数据为一行，分割符为&，转义为&amp,每次分割为在此行创建一个单元格
     * @param dataInfo
     * @return
     */
    @Override
    public int creatDataInfoByStr(List<String> dataInfo) {
        //CellRangeAddress region = new CellRangeAddress(0, 0, 0, heads.size()-1);
        //sheet.addMergedRegion(region);
        for(String infoItem : dataInfo){
            try{
                List<JSONObject> jsonArray = JSONArray.parseArray(infoItem,JSONObject.class);
                jsonArray.forEach(colParam->{
                    JSONArray mergeColumn = colParam.getJSONArray("merge");
                    String styleName = colParam.getString("style");
                    try {
                        CellRangeAddress region = new CellRangeAddress(mergeColumn.getInteger(0),
                                mergeColumn.getInteger(1), mergeColumn.getInteger(2), mergeColumn.getInteger(3));
                        sheet.addMergedRegion(region);
                        Row row = sheet.getRow(mergeColumn.getInteger(0));
                        if(row == null){
                            row = sheet.createRow(startRows);
                        }
                        Cell cell = row.createCell(mergeColumn.getInteger(2));
                        cell.setCellStyle(styles.get(styleName==null?"infoStyleBase":styleName));//设置样式
                        cell.setCellValue(colParam.getString("data"));
                    }catch (Exception ex){

                    }
                });

            }catch (Exception ex) {
                Row row = sheet.createRow(startRows);//创建一行数据
                String items[] = infoItem.split("&");
                for (int i = 0; i < items.length; i++) {//输出说明数据
                    Cell cell = row.createCell(i + startCols);
                    cell.setCellStyle(styles.get("infoStyleBase"));//设置样式
                    cell.setCellValue(items[i]);
                }

            }
            startRows++;
        }
        return startRows;
    }

    @Override
    public int creatDataHeadByListStr(List<String> dataHead) {
        Row row = sheet.createRow(startRows);//创建新的一行数据
        for(int i=0;i<dataHead.size();i++) {
            sheet.setColumnWidth(i,20*256);
            String[] keys = dataHead.get(i).split("&");
            heads.add(keys[0]);//把列名放到集合中
            Cell cell = row.createCell(i+startCols);
            if(keys.length>=2){//第二项存在
                cell.setCellStyle(styles.get("headStyleBase"));//设置样式
                cell.setCellValue(keys[1]);
            }

        }
        // 合并单元格

        startRows++;
        return startRows;
    }

    @Override
    public int creatDataBodyByListMap(List<HashMap<String, String>> dataBody) {
        for(int i=0;i<dataBody.size();i++) {
            Row row = sheet.createRow(startRows);//因为第一行为题目，故从第二行开始输出正文
            for(int j=0;j<heads.size();j++) {
                Cell cell = row.createCell(startCols+j);
                cell.setCellStyle(styles.get("bodyStyleBase"));//设置样式
                try {
                    cell.setCellValue((dataBody.get(i).get(heads.get(j))).toString());
                } catch (NullPointerException e){
                    cell.setCellValue("");
                }
            }
            startRows ++;
        }
        return startRows;
    }

    @Override
    public String outToFile() {
        String realExcelName = this.getFileName() + Suffix;
        String fullPath = getFileDir() + realExcelName;
        try {
            FileOutputStream fileOut = new FileOutputStream(fullPath);
            wb.write(fileOut);
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fullPath;
    }

    public int getStartRows() {
        return startRows;
    }

    public void setStartRows(int startRows) {
        this.startRows = startRows;
    }

    public int getStartCols() {
        return startCols;
    }

    public void setStartCols(int startCols) {
        this.startCols = startCols;
    }

    public String getSuffix() {
        return Suffix;
    }

    public void setSuffix(String suffix) {
        Suffix = suffix;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    public HashMap<String, CellStyle> getStyles() {
        return styles;
    }

    public void setStyles(HashMap<String, CellStyle> styles) {
        this.styles = styles;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
