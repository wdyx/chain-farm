package com.util.exportout.GenerateFileUtil;


import com.util.exportout.IFileData;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 */
public class GenerateFile {

    protected String fileType;
    protected String returnFilePath;
    protected InputStream returnFileStream;
    protected HashMap inputDataMap;
    protected HashMap<String, String> GenerateFileSuffixMap;
    protected IFileData fileData;


    public GenerateFile(String fileType, HashMap inputDataMap) {
        this.fileType = fileType;
        this.inputDataMap = inputDataMap;

        this.init();
        this.generate();
    }


    public String getReturnFilePath() {
        return returnFilePath;
    }

    public InputStream getReturnFileStream() {
        FileInputStream fileInputStream = null;
            try {
                if(returnFilePath!=null){
                    fileInputStream =  new FileInputStream(returnFilePath);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        return fileInputStream;
    }


    public void init() {
        initGenerateFileSuffixMap();
    }

    public void initGenerateFileSuffixMap() {
        if (GenerateFileSuffixMap == null) {
            GenerateFileSuffixMap = new HashMap<String, String>();
        }

        GenerateFileSuffixMap.put("xls", "excel2003");
        GenerateFileSuffixMap.put("xlsx", "excel2007");
        GenerateFileSuffixMap.put("pdf", "pdfManager");
        GenerateFileSuffixMap.put("csv", "csvManager");
        GenerateFileSuffixMap.put("html", "htmlManager");
    }



    private void generate() {
        try {

            String fileName =inputDataMap.get("defineFileName")+"";
            fileName="report_download_temp";
//            fileData.init(FileNameUtil.fileNameReplaceUnLimitSymble(fileName));//web中可以不设置文件名 在action中定义
            fileData.init(fileName);//web中可以不设置文件名 在action中定义
            fileData.creatDataInfoByStr((List<String>) inputDataMap.get("infoList"));
            fileData.creatDataHeadByListStr((List<String>) inputDataMap.get("headList"));
            fileData.creatDataBodyByListMap((List<HashMap<String, String>>) inputDataMap.get("bodyList"));
            returnFilePath = fileData.outToFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
