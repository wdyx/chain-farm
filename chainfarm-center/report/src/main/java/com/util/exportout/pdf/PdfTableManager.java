package com.util.exportout.pdf;

import com.lowagie.text.Element;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class PdfTableManager {

    public PdfPTable createTableForInfo(int colNumber,int width){
        PdfPTable table = new PdfPTable(colNumber);
        try{
            table.setTotalWidth(width);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
        }catch(Exception e){
            e.printStackTrace();
        }
        return table;
    }

}
