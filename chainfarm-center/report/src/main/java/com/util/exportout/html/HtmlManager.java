package com.util.exportout.html;

import com.lowagie.text.DocumentException;
import com.util.exportout.FileManager;
import com.util.exportout.IFileData;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-10-10
 * Time: 下午1:20
 * To change this template use File | Settings | File Templates.
 */
public class HtmlManager extends FileManager implements IFileData {
    protected List<String> heads = new ArrayList<String>();//记录头部数据
    protected String fileName = "html";
    protected String contentStr = "";//输出内容
    public HtmlManager(){

    }

    public HtmlManager(String fName){
        this.setFileName(fName);
    }
    @Override
    public void init(String fileName) {
        this.setFileName(fileName);
    }

    public static void main(String[] avg){
        HtmlManager htmlManager = new HtmlManager();
        List<String> info = new ArrayList<String>();
        info.add("d&f&H&f");
        info.add("e&f");
        info.add("e&f&k&r&t&y");

        List<String> headinfo2 = new ArrayList<String>();
        headinfo2.add("A&你很骄傲");
        headinfo2.add("B&我很骄傲");


        List<HashMap<String,String>>  bodyInfo = new ArrayList<HashMap<String, String>>();

        HashMap<String,String>  my1 = new HashMap<String, String>();
        my1.put("A","我是A1");
        my1.put("B","我是A2");
        bodyInfo.add(my1);


        HashMap<String,String>  my2 = new HashMap<String, String>();
        my2.put("A","我是B1");
        my2.put("B","我是B2");
        bodyInfo.add(my2);



        try{
            htmlManager.creatDataInfoByStr(info);
            htmlManager.creatDataHeadByListStr(headinfo2);
            htmlManager.creatDataBodyByListMap(bodyInfo);
            htmlManager.outToFile();
        }catch (DocumentException e){
            e.printStackTrace();
        }
    }

    @Override
    public int creatDataInfoByStr(List<String> dataInfo) throws DocumentException {
        //1创建头部信息
        //2引用css
        HtmlCssManager htmlCssManager = new HtmlCssManager();
        contentStr = "<!doctype html>" + "\r\n";
        contentStr = contentStr+"<html>" + "\r\n";
        contentStr = contentStr+"   <head>" + "\r\n";
        contentStr = contentStr+"   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" + "\r\n";
        contentStr = contentStr+"   <title>" + this.getFileName() + "</title>"+ "\r\n";
        contentStr = contentStr+htmlCssManager.getBaseStyle()+ "\r\n";//引入CSS
        contentStr = contentStr+"   </head>"+ "\r\n";
        contentStr = contentStr+"   <body>"+ "\r\n";
        int maxcolSpan = 0;
        //获取头部最大的列数
        for(String infoItem : dataInfo){
            int tmpcolNum = 0;
            String tmp[] = infoItem.split("&");
            tmpcolNum = tmp.length;
            if(tmpcolNum > maxcolSpan){
                maxcolSpan = tmpcolNum;
            }
        }
        String tableStr = "";
        String tableContent = "";
        tableStr = tableStr + "<table class=\"infoStyle\">" + "\r\n";
        //判断是否能被最大的整除
        for(String infoItem : dataInfo){
            int tmpcolNum = 0;
            int colSpanNum = 0;
            String tmp[] = infoItem.split("&");
            tmpcolNum = tmp.length;
            if(maxcolSpan%tmpcolNum == 0){ //如果可以被整除，那么就自动平均分布
                colSpanNum = maxcolSpan/tmpcolNum;
            }
            String trStr = "";//记录每行数据
            trStr = trStr + "<tr>"+  "\r\n";
            for(int i=0;i<tmp.length;i++){
                trStr = trStr + "<td";
                if(colSpanNum == 0){
                    if(i==tmp.length-1){//否则合并最后一列
                        int lastColSpan = 0;
                        lastColSpan = maxcolSpan - (tmp.length-1);
                        trStr = trStr + " colspan="+lastColSpan;
                    }
                }else if(colSpanNum == 1){
                    trStr = trStr;
                }else{
                    trStr = trStr + " colspan="+colSpanNum;
                }
                trStr = trStr + ">" + tmp[i] + "</td>" +  "\r\n";
            }
            trStr = trStr + "</tr>"+  "\r\n";
            tableContent = tableContent + trStr;
        }
        tableStr = tableStr + tableContent +  "</table>";
        contentStr = contentStr + tableStr  +  "\r\n";
        return 0;
    }

    @Override
    public int creatDataHeadByListStr(List<String> dataHead) throws DocumentException {
        String tableStr = "";
        String tableContent = "";
        tableStr = tableStr + "<table class=\"mainStyle\">" + "\r\n";
        tableStr = tableStr + "<thead>" + "\r\n";
        tableStr = tableStr + "<tr>" + "\r\n";
        for(String headItem : dataHead){
            String[] itmArr = headItem.split("&");
            heads.add(itmArr[0]);
            String trStr = "";//记录每行数据
            trStr = trStr + "<td>" + itmArr[1] + "</td>" + "\r\n";
            tableContent = tableContent + trStr;
        }
        tableStr = tableStr + tableContent + "</tr>" + "\r\n";
        tableStr = tableStr + "</thead>" + "\r\n";
        contentStr = contentStr + tableStr  +  "\r\n";
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int creatDataBodyByListMap(List<HashMap<String, String>> dataBody) throws DocumentException {
        String tableStr = "";
        String tableContent = "";
        tableStr = tableStr + "<tbody>" + "\r\n";
        for(HashMap<String, String> bodyItem : dataBody){
            String trStr = "";//记录每行数据
            trStr = trStr + "<tr>"+  "\r\n";
            for(String key:heads){//根据头部进行输出，而不是身体数据
                trStr = trStr + "<td>" + bodyItem.get(key).toString() + "</td>" + "\r\n";
            }
            trStr = trStr + "</tr>"+  "\r\n";
            tableContent = tableContent + trStr;
        }
        tableStr = tableStr + tableContent + "</tbody>" + "\r\n";
        tableStr = tableStr + "</table>";
        contentStr = contentStr + tableStr  +  "\r\n";
        return 0;
    }

    @Override
    public String outToFile() throws DocumentException {
        contentStr = contentStr + "</body>" + "\r\n";
        contentStr = contentStr + "</html>";
        String fullName = getFileDir() + this.getFileName() + ".html";
        try{
            FileOutputStream fos=new FileOutputStream(fullName);
            OutputStreamWriter osw= new OutputStreamWriter(fos);
            BufferedWriter bw=new BufferedWriter(osw);
            bw.write(contentStr);
            bw.close();
            osw.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return fullName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}

