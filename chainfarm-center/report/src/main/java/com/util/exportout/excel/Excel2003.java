package com.util.exportout.excel;

import com.util.exportout.excel.abstr.ExcelManager;
import com.util.exportout.excel.style.ExcelStyleManager;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class Excel2003 extends ExcelManager {
    public Excel2003(){
        this.creatWorkBook();
        this.init();
    }
    public Excel2003(String fName){
        super(fName);
        this.creatWorkBook();
        this.init();
    }
    @Override
    public Workbook creatWorkBook() {
        wb = new HSSFWorkbook();

        super.setSuffix(".xls");
        return wb;
    }
    public void init(){
        Sheet sheet = wb.createSheet();
        super.setSheet(sheet);
        ExcelStyleManager excelStyleManager = new ExcelStyleManager();
        super.setStyles(excelStyleManager.getBaseStyle(wb));
    }


}
