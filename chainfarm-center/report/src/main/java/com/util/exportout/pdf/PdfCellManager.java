package com.util.exportout.pdf;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import java.awt.*;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: suhongliang
 * Date: 13-8-8
 * Time: 下午1:29
 * To change this template use File | Settings | File Templates.
 */
public class PdfCellManager {
    private HashMap<String, Font> pdfFonts = new HashMap<String, Font>();
    public PdfCellManager(){
        PdfFontManager  pdfFontManager = new PdfFontManager();
        pdfFonts = pdfFontManager.getBaseFonts();
    }

    public PdfPCell createCellForInfo(String value){
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setPhrase(new Phrase(value,pdfFonts.get("8FOLD")));
        return cell;
    }
    public PdfPCell createCellForInfo(String value,int colspan){
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value,pdfFonts.get("8FOLD")));
        return cell;
    }
    public PdfPCell createCellCenterBlue(String value){
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(Color.blue);
        cell.setPhrase(new Phrase(value,pdfFonts.get("8FOLD")));
        return cell;
    }
    public PdfPCell createCellCenterWrite(String value){
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(Color.white);
        cell.setPhrase(new Phrase(value,pdfFonts.get("8FOLD")));
        return cell;
    }
}
