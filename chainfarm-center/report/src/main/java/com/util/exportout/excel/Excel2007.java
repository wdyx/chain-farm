package com.util.exportout.excel;

import com.util.exportout.excel.abstr.ExcelManager;
import com.util.exportout.excel.style.ExcelStyleManager;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Created with IntelliJ IDEA.
 * To change this template use File | Settings | File Templates.
 */
public class Excel2007 extends ExcelManager {
    public Excel2007() throws Exception {
        this.creatWorkBook();
        this.init();
    }
    public Excel2007(String fName) throws Exception{
        super(fName);
        this.creatWorkBook();
        this.init();
    }
    @Override
    public Workbook creatWorkBook()
    {
        try {
            wb = new XSSFWorkbook();
            super.setSuffix(".xlsx");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wb;
    }
    public void init(){
        super.setSheet(wb.createSheet());
        ExcelStyleManager excelStyleManager = new ExcelStyleManager();
        super.setStyles(excelStyleManager.getBaseStyle(wb));
    }
}
