package com.util;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 工具类：日期转换
 */
public class DateConvertUtil {

    /**
     * 将13位时间戳转换为日期时间
     * @return yyyy-MM-dd HH:mm:ss.SSS
     */
    public static String stampToyyyyMMddHHmmssSSS(String stamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.CHINA);
        long lt = new Long(stamp);
        Date date = new Date(lt);
        String res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 将13位时间戳转换为时间
     * @param stamp
     * @return HH:mm:ss.SSS
     */
    public static String stampToHHmmssSSS(String stamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS", Locale.CHINA);
        long lt = new Long(stamp);
        Date date = new Date(lt);
        String res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 将13位时间戳转换为时间（HH）
     * 时间格式：HHmm
     * @param stamp
     * @return 00-23
     */
    public static String stampToHH(String stamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH", Locale.CHINA);
        long lt = new Long(stamp);
        Date date = new Date(lt);
        String res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 将时间字符串转换为13位时间戳
     * @param timeStr（yyyy-MM-dd HH:mm:ss.SSS）
     * @return 1530687199834
     */
    public static String timeStr2TimeStamp(String timeStr) {
        String stamp = "";
        try {
            stamp = String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.CHINA).parse(timeStr).getTime()) + "";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stamp;
    }

    /**
     * 将时间字符串转换为17位时间戳字符串
     * @param timeStr（yyyy-MM-dd HH:mm:ss.SSS）
     * @return yyyyMMddHHmmssSSS
     */
    public static String timeStr2TimeStampStr(String timeStr) {
        return timeStr.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").replaceAll(":", "");
    }
    public static String timeStr2TimeStampStr6(String timeStr) {
        String s = timeStr.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").replaceAll(":", "");
        return s.substring(0, 6);
    }
    public static String timeStr2TimeStampStr8(String timeStr) {
        String s = timeStr.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\.", "").replaceAll(":", "");
        return s.substring(0, 8);
    }

    /**
     * 将17位时间戳字符串转换为时间字符串
     * @param timeStr（yyyyMMddHHmmssSSS）
     * @return yyyy-MM-dd HH:mm:ss.SSS
     */
    public static String TimeStampStr2timeStr(String timeStr) {
        if(StringUtils.isNotEmpty(timeStr) && timeStr.length() == 17){
            return timeStr.substring(0,4) + "-" + timeStr.substring(4,6) + "-" + timeStr.substring(6,8) + " " + timeStr.substring(8,10) + ":" + timeStr.substring(10,12) + ":" + timeStr.substring(12,14) + "." + timeStr.substring(14,17);
        }else{
            return timeStr;
        }
    }

    /**
     * 将17位时间戳字符串转换为时间字符串
     * @param timeStr（yyyyMMddHHmmssSSS）
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String TimeStampStr2timeStr14(String timeStr) {
        if(StringUtils.isNotEmpty(timeStr) && timeStr.length() == 18){
            return timeStr.substring(0,4) + "-" + timeStr.substring(4,6) + "-" + timeStr.substring(6,8) + " " + timeStr.substring(8,10) + ":" + timeStr.substring(10,12) + ":" + timeStr.substring(12,14)+":"+timeStr.substring(14,18);
        }else if(StringUtils.isNotEmpty(timeStr) && timeStr.length() == 14){
            return timeStr.substring(0,4) + "-" + timeStr.substring(4,6) + "-" + timeStr.substring(6,8) + " " + timeStr.substring(8,10) + ":" + timeStr.substring(10,12)+":"+timeStr.substring(12,14);
        }else if(StringUtils.isNotEmpty(timeStr) && timeStr.length() == 17){
            return timeStr.substring(0,4) + "-" + timeStr.substring(4,6) + "-" + timeStr.substring(6,8) + " " + timeStr.substring(8,10) + ":" + timeStr.substring(10,12) + ":" + timeStr.substring(12,14)+":"+timeStr.substring(14,17);
        } else {
            return timeStr;
        }
    }

    /**
     * 将日期转为字符串
     * @param date
     * @return yyyy-MM-dd HH:mm:ss.SSS
     */
    public static String date2String(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.CHINA);
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }
    public static String date2String19(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    /**
     * 将日期转为17位Long
     * @param date
     * @return yyyyMMddHHmmssSSS
     */
    public static Long date2Long(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.CHINA);
        String dateStr = simpleDateFormat.format(date);
        if(StringUtils.isNotEmpty(dateStr)){
            return Long.parseLong(dateStr);
        }else{
            return 0L;
        }
    }

    /**
     *  @Author: JOKE
     *  @Date: 17:45 2018/6/29
     *  @Description: 将时间字符串转为Long
    */
    public static Long string2Long(String timeStr){
        if (StringUtils.isBlank(timeStr)){
            return 0L;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        try {
            Date date=simpleDateFormat.parse(timeStr);
            return date2Long(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }

    /**
     *@Author: joke
     *@Date: 12:03 2018/8/7
     *@Description: 计算传入日期与1970年1月1日天数差
    */
    public static Long calculateTimeDifferenceByChronoUnit(String dayStr){
        try {
            SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
            long from1 = simpleFormat.parse("1970-01-01").getTime();
            long to1 = simpleFormat.parse(dayStr).getTime();
            /*天数差*/
            long daysDiff = (long) ((to1 - from1) / (1000 * 60 * 60 * 24));
            System.out.println("两个时间之间的天数差为：" + daysDiff);
            return daysDiff;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }

    /**
     *@Author: joke
     *@Date: 10:19 2018/8/8
     *@Description: 使用Calendar类计算传入日期与1970年1月1日天数差
    */
    public static Long daysBetween(String dayStr){
        try{
            SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
            String fromDateStr="1970-01-01";
            Calendar cal = Calendar.getInstance();
            cal.setTime(simpleFormat.parse(fromDateStr));
            long time1 = cal.getTimeInMillis();
            cal.setTime(simpleFormat.parse(dayStr));
            long time2 = cal.getTimeInMillis();
            long between_days=(time2-time1)/(1000*3600*24);
            System.out.println("两个时间之间的天数差为："+between_days);
            return between_days;
        }catch (Exception e){
            e.printStackTrace();
            return 0L;
        }
    }



  public static void main(String[] args) {
//        Date d = new Date();
//        Long dateTime = d.getTime();
//        System.out.println(dateTime+"");
//        System.out.println(stampToyyyyMMddHHmmssSSS(dateTime+""));
//        System.out.println(stampToHHmmssSSS(dateTime+""));
//        System.out.println(stampToHH(dateTime+""));
//        System.out.println(timeStr2TimeStamp("2018-06-25 13:34:55.678"));
//        System.out.println(timeStr2TimeStampStr("2018-06-25 13:34:55.678"));
//        System.out.println(TimeStampStr2timeStr("20180625133455678"));
//        System.out.println(TimeStampStr2timeStr14("20180625133455678"));
//        System.out.println(date2String(d));
//        System.out.println(date2Long(d));
          calculateTimeDifferenceByChronoUnit("2018-08-01");

        daysBetween("2018-08-01");

  }

}
