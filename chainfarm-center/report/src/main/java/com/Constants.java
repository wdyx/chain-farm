package com;

/**
 *
 */
public class Constants {

    /** Word版本 */
    public static final String word_2007 = "word 2007";
    /** Word版本 */
    public static final String word_2003 = "word 2003";


    /** Excel版本 */
    public static final String excel_2007 = "excel 2007";
    /** Excel版本 */
    public static final String excel_2003 = "excel 2003";



    /** 分隔符 */
    public static final String SPLIT = ",";
    public static final String SPLIT1 = "#";
    public static final String SPLIT2 = "*";

    /** 条件 - 查询的SQL条件 */
    public static final String where = "where";
    /** 条件 - 有效行数（开始）*/
    public static final String row_s = "row-start";
    /** 条件 - 有效行数（结束）*/
    public static final String row_e = "row-end";
    /** 条件 - 有效列数（开始）*/
    public static final String col_s = "col-start";
    /** 条件 - 有效列数（结束）*/
    public static final String col_e = "col-end";
    /** 条件 - 字段名称*/
    public static final String field_name = "field-name";


    /** 查询SQL */
    public static final String SQL = "@SQL_";
    /** 查询资源ID-多个以逗号分隔 */
    public static final String RESID = "@RESID_";
    /** 查询资源类型-多个以逗号分隔 */
    public static final String RESTYPE = "@RESTYPE_";

    /** 展示指标-多个以逗号分隔 */
    public static final String KPI = "@KPI_";
    /** 展示字段-多个以逗号分隔 */
    public static final String FIELD = "@FIELD_";

    /** 手动输入 */
    public static final String INPUT = "@INPUT_";
    /** 下拉列表-多个以逗号分隔 */
    public static final String SELECT = "@SELECT_";

    /** 循环多少行-以逗号分隔 */
    public static final String LOOP = "@LOOP_";
    /** 列区间-以逗号分隔 */
    public static final String COL = "@COL_";


    /** 求和函数 */
    public static final String FUN_SUM = "SUM";
    /** 求平局数函数 */
    public static final String FUN_AVG = "AVERAGE";
    /** 求最大函数 */
    public static final String FUN_MAX = "MAX";
    /** 求最小函数 */
    public static final String FUN_MIN = "MIN";
    /** 判断函数 */
    public static final String FUN_IF = "IF";
    /** 统计函数 */
    public static final String FUN_COUNT = "COUNT";


    /**
     * 任务周期
     */
    public static final String CHECK_FREQUENCY_HOUR = "hour";//每小时
    public static final String CHECK_FREQUENCY_DAY = "day";//每天
    public static final String CHECK_FREQUENCY_WEEK = "week";//每周
    public static final String CHECK_FREQUENCY_MONTH = "month";//每月
    public static final String CHECK_FREQUENCY_QUARTER = "quarter";//每季度
    public static final String CHECK_FREQUENCY_YEAR = "year";//每年

}
