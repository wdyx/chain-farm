package com.schedule.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 09 39
 * @Describe
 */
@Data
@Entity
@Table(name = "b_report_rule")
public class ReportRule {
    @Id
    private String id;
    @Column(name = "ruleName")
    private String ruleName;
    @Column(name = "reportTitleRule")
    private String reportTitleRule;
    @Column(name = "frequency")
    private String frequency;
    @Column(name = "docCode")
    private String docCode;//赵鹏军|20181104修改|增加文档类型字段，依照规范巡检报告要求
    @Column(name = "createTime")
    private Date createTime;
    @Column(name = "lastUpdateTime")
    private Date lastUpdateTime;

    // wt add  2018.11.27 个性化报表字段
    @Column(name = "customType")
    private String customType;
}
