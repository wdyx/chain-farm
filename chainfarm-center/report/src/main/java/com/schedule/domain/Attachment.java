package com.schedule.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 23 02
 * @Describe
 */
@Data
@Entity
@Table(name = "b_attachment")
public class Attachment {

    @Id
    String id;
    @Column(name = "name")
    String name;
    @Column(name = "path")
    String path;
    @Column(name = "suffix")
    String suffix;
    @Column(name = "docCode")
    String docCode;
    @Column(name = "extend")
    String extend;
    @Column(name = "createTime")
    Date createTime;
    @Column(name = "ruleId")
    String ruleId;
}
