package com.schedule.domain;

import lombok.Data;

import javax.persistence.Transient;

@Data
public class ResultRealtimeCollector {
    private String resId; // 资源Id
    private String resName;// 资源名称
    private String kpiId;// 指标Id
    private String kpiName;// 指标名称
    private String kpiValue;// 指标值
    private String flag;// 二级资源标识
    private String ip;
    private String resType;// 资源类型
    private String os;// 操作系统
    private String version;// 版本信息
    private String admin; // 管理员
    private String busName; // 业务名称
    private String busIp; // 业务IP
    private String managerIp; // 管理IP
    private String hostName; // 主机名称


    // 非持久化属性
    /** 报表生成路径 */
    @Transient
    private int index;
}