package com.schedule.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 09 42
 * @Describe
 */

@Data
@Entity
@Table(name = "b_report_rule_task")
public class ReportRuleTask{

    @Id
    private String id;

    @Column(name = "taskId")
    private String taskId;
    @Column(name = "ruleId")
    private String ruleId;

}
