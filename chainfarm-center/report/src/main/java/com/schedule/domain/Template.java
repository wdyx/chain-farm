package com.schedule.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 */
@Data
@Entity
@Table(name = "b_template")
public class Template {

    @Id
    private String id;// 主键Id
    private String code;//
    private String name;//
    private String path;//
    private String cron;//
    private String createUserId;//
    private String info;//
    private String userIds;//
    private String desc;//
    private String docCode;//
    private String suffix;// 后缀
    private String createTime;//
    private String updateTime;//



    // 非持久化属性
    /** 报表生成路径 */
    @Transient
    private String reportPath;
    /** 报表模板路径前缀 */
    @Transient
    private String reportTemplatePath;
    /** 默认SQL */
    @Transient
    private String defaultSQL;

}
