package com.report.repository;

import com.schedule.domain.ReportRuleTask;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 11 56
 * @Describe
 */
public interface ReportRuleTaskRepository extends CrudRepository<ReportRuleTask,String> {

    public List<ReportRuleTask> findAllByRuleIdEquals(String ruleId);
}
