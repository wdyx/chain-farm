package com.report.repository;

import com.schedule.domain.ReportRule;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 11 55
 * @Describe
 */
public interface ReportRuleRepository extends JpaSpecificationExecutor<ReportRule>,CrudRepository<ReportRule,String> {
}
