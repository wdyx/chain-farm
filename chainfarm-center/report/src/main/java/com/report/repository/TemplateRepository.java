package com.report.repository;

import com.schedule.domain.Template;
import org.springframework.data.repository.CrudRepository;

/**
 * 模板信息
 */
public interface TemplateRepository extends CrudRepository<Template,String> {
}
