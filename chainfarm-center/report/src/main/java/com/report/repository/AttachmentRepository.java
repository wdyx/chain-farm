package com.report.repository;

import com.schedule.domain.Attachment;
import org.springframework.data.repository.CrudRepository;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 23 05
 * @Describe
 */
public interface AttachmentRepository extends CrudRepository<Attachment,String> {
}
