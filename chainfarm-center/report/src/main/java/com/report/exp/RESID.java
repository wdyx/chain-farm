package com.report.exp;

import com.Constants;
import com.SpringBeanLoader;
import com.report.service.ReportLoadDataService;
import com.schedule.domain.ResultRealtimeCollector;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
//@Scope("singleton")// 单例模式
public class RESID extends BaseExp{

    /**
     * 单例
     */
    private static class SingletonInstance {
        private static final RESID INSTANCE = new RESID();
    }

    public static RESID getInstance() {
        return SingletonInstance.INSTANCE;
    }


    public RESID(){
        this.expName = Constants.RESID;
    }


    /**
     * 表达式初始化
     */
    @Override
    public void init(String exp) throws Exception {
        super.init(exp);

        // 拼接解析字符串，拼接SQL
        String field = "t1.resId";
        this.setSQL(field);
    }

}
