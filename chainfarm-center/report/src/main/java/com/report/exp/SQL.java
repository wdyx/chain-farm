package com.report.exp;

import com.Constants;
import com.schedule.domain.ResultRealtimeCollector;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.Constants.*;

/**
 *
 */
@Service
public class SQL extends BaseExp{

    /**
     * 单例
     */
    private static class SingletonInstance {
        private static final SQL INSTANCE = new SQL();
    }

    public static SQL getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private SQL(){
        this.expName = SQL;
    }

    /**
     * 表达式初始化
     */
    @Override
    public void init(String exp) throws Exception {
        super.init(exp);

        this.sql = this.params.get(where);

    }
}
