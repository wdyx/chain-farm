package com.report.exp;

import com.Constants;
import com.schedule.domain.ResultRealtimeCollector;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表达式工具类
 */
//@Service
public class ExpUtil {

    private static KPI kpi = KPI.getInstance();
    private static RESID resId = RESID.getInstance();
    private static RETYPE resType = RETYPE.getInstance();
    private static SQL sql = SQL.getInstance();
    private static FIELD field = FIELD.getInstance();


    private BaseExp baseExp = null;

    public Map<String,Object> getVal(String exp, String defaultSQL) throws
            Exception {
        if (StringUtils.isNotBlank(exp)) {
            if (exp.startsWith(kpi.expName)) {
                baseExp = kpi;
            } else if (exp.startsWith(resId.expName)) {
                baseExp = resId;
            } else if (exp.startsWith(resType.expName)) {
                baseExp = resType;
            } else if (exp.startsWith(sql.expName)) {
                baseExp = sql;
            } else if(exp.startsWith(field.expName)){
                baseExp = field;
            } else if(exp.startsWith(Constants.INPUT) || exp.startsWith(Constants.SELECT)){
                return null;
            } else {
                throw new Exception("未定义的语法：" + exp);
            }
            // 设置默认查询SQL
            baseExp.setProperty(defaultSQL);
            // 初始化当前衣服啊
            baseExp.init(exp);
            // 解析获取数据
            Map<String, List<ResultRealtimeCollector>> list = baseExp.getList();

            Map<String,Object> result = new HashMap<>();
            result.put("params",baseExp.params);
            result.put("list",list);

            return  result;
        } else {
            throw new NullPointerException("表达式为空！");
        }
    }

}
