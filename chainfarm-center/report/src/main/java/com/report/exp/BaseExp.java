package com.report.exp;

import com.Constants;
import com.SpringBeanLoader;
import com.report.service.ReportLoadDataService;
import com.schedule.domain.ResultRealtimeCollector;
import com.util.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.Constants.*;

/**
 * 表达式父类
 */
public abstract class BaseExp {

    /**
     * 表达式名称
     */
    public String expName;
    /**
     * 表达式参数
     */
    public Map<String,String> params = new HashMap<>();
    /**
     * 表达式解析后的SQL
     */
    public String sql;

    /**
     * 默认的查询sql
     */
    protected String default_sql;

    /**
     * 当前表达式的其他条件
     */
    protected String[] otherParam;

    /**
     * 设置默认SQL
     * @param defaultSql
     */
    public void setProperty(String defaultSql){
        this.default_sql = defaultSql;
    }

    /**
     * 表达式初始化
     */
    public void init(String exp) throws Exception{
        if(!Utils.isExp(exp, this.expName)){
            throw new Exception("当前表达式不能解析！" + exp);
        }

        // 获取表达式后的参数
        String[] split = exp.split(this.expName);
        String expParam = split[1];
        if(expParam.indexOf(SPLIT1) >= 0){
            // 多个表达式
            String[] expArr = expParam.split(SPLIT1);
            params.put(where,expArr[0]);
            otherParam = expArr;
        } else {
            otherParam = split;
        }
    }

    /**
     * 表达式初始化
     */
    public void init(String exp,List<ResultRealtimeCollector> list) throws Exception{
       this.init(exp);
    }

    /**
     * 获取集合
     * @return
     */
    public Map<String, List<ResultRealtimeCollector>> getList() throws Exception {
        ReportLoadDataService reportLoadDataService = SpringBeanLoader.getSpringBean(ReportLoadDataService.class);;
        List<ResultRealtimeCollector> list = reportLoadDataService.loadData(this.sql);


        // 将数据按照资源ID分组
        Map<String, List<ResultRealtimeCollector>> listMap = list.stream().collect(Collectors
                .groupingBy(ResultRealtimeCollector::getResId));


        for (int i = 1; i < otherParam.length; i++) {
            String item = otherParam[i];
            if(item.indexOf(SPLIT) != -1){
                String[] itemParam = item.split(SPLIT);
                String start = itemParam[0];
                String end = itemParam[1];
                String strEnd = itemParam[1];

                if(SPLIT2.equals(strEnd)){
                    end = String.valueOf(listMap.keySet().size());
                }

                if(item.startsWith(LOOP)){
                    // 行循环开始结束
                    this.params.put(row_s,start.replace(LOOP,""));
                    this.params.put(row_e,end);
                } else if(item.startsWith(COL)){
                    // 列循环开始结束
                    this.params.put(col_s,start.replace(COL,""));
                    this.params.put(col_e,end);
                }
            } else {
                ExpUtil expUtil = new ExpUtil();
                Map<String, Object> map = expUtil.getVal(item, this.default_sql);
                if(map != null){

                    // 表达式参数
                    Map<String, String> params = (Map<String, String>) map.get("params");

                    this.params.putAll(params);
                }
            }
        }

        return listMap;
    }

    /**
     * 拼接sql
     *
     * @return
     */
    protected void setSQL(String field) {
        String[] arr = this.params.get(where).split(SPLIT);
        int size = arr.length;

        String where = field + " in (";
        for (int i = 0; i < size; i++) {
            String item = arr[i];
            where += "'" + item + "'";
            if (i < size - 1) {
                where += ",";
            }
        }
        where += ")";
        this.sql = String.format(this.default_sql, where);
    }

}
