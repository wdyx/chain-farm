package com.report.exp;

import com.Constants;
import com.schedule.domain.ResultRealtimeCollector;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
public class KPI extends BaseExp {

    /**
     * 单例
     */
    private static class SingletonInstance {
        private static final KPI INSTANCE = new KPI();
    }

    public static KPI getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private KPI() {
        this.expName = Constants.KPI;
    }

    /**
     * 表达式初始化
     */
    @Override
    public void init(String exp) throws Exception {
        super.init(exp);

    }

    /**
     * 获取集合
     *
     * @return
     */
    @Override
    public Map<String, List<ResultRealtimeCollector>> getList() {
        return null;
    }
}
