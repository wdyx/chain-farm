package com.report.exp;

import com.Constants;
import com.schedule.domain.ResultRealtimeCollector;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.Constants.SQL;
import static com.Constants.field_name;
import static com.Constants.where;

/**
 *
 */
@Service
public class FIELD extends BaseExp{

    /**
     * 单例
     */
    private static class SingletonInstance {
        private static final FIELD INSTANCE = new FIELD();
    }

    public static FIELD getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private FIELD(){
        this.expName = Constants.FIELD;
    }

    /**
     * 表达式初始化
     */
    @Override
    public void init(String exp) throws Exception {
        super.init(exp);
        this.params.put(field_name,this.otherParam[1]);
    }

    /**
     * 获取集合
     *
     * @return
     */
    @Override
    public Map<String, List<ResultRealtimeCollector>> getList() {
        return null;
    }
}
