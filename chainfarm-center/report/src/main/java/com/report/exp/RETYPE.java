package com.report.exp;

import com.Constants;
import com.schedule.domain.ResultRealtimeCollector;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class RETYPE extends BaseExp{

    /**
     * 单例
     */
    private static class SingletonInstance {
        private static final RETYPE INSTANCE = new RETYPE();
    }

    public static RETYPE getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private RETYPE(){
        this.expName = Constants.RESTYPE;
    }

    /**
     * 表达式初始化
     */
    @Override
    public void init(String exp) throws Exception {
       super.init(exp);

        // 拼接解析字符串，拼接SQL
        String field = "t1.resType";
        this.setSQL(field);

    }


}
