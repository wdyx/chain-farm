package com.report.service;

import com.schedule.domain.ReportRule;
import com.schedule.domain.ReportRuleTask;
import com.schedule.domain.Template;
import com.report.repository.ReportRuleRepository;
import com.report.repository.ReportRuleTaskRepository;
import com.report.repository.TemplateRepository;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;

/**
 * @Auther zhangdajun
 * @CreateDate 2018/11/3 12 40
 * @Describe
 */
@Service
public class ReportRuleService {

//    @Autowired
//    ReportRuleRepository reportRuleRepository;

    @Autowired
    protected TemplateRepository templateRepository;

    @Autowired
    ReportRuleTaskRepository reportRuleTaskRepository;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Transactional
    public Iterator<Template> loadLastOneMinuteReportRule(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            String sql = "select t.* from b_template t where DATE_SUB(NOW(),INTERVAL 1 MINUTE) < t.updateTime";

            Query query = entityManager.createNativeQuery(sql)
                    .unwrap(NativeQuery.class)
                    .addScalar("id", StandardBasicTypes.STRING)
                    .addScalar("code", StandardBasicTypes.STRING)
                    .addScalar("name", StandardBasicTypes.STRING)
                    .addScalar("path", StandardBasicTypes.STRING)
                    .addScalar("cron", StandardBasicTypes.STRING)
                    .addScalar("info", StandardBasicTypes.STRING)
                    .addScalar("userIds", StandardBasicTypes.STRING)
                    .addScalar("desc", StandardBasicTypes.STRING)
                    .addScalar("docCode", StandardBasicTypes.STRING)
                    .addScalar("createUserId", StandardBasicTypes.STRING)
                    .addScalar("createTime", StandardBasicTypes.TIMESTAMP)
                    .addScalar("updateTime", StandardBasicTypes.TIMESTAMP);
            query.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(ReportRule.class));
            List<Template> reportRules = query.getResultList();
            if (reportRules != null) {
                return reportRules.iterator();
            }
            return null;
        }finally {
            EntityManagerFactoryUtils.closeEntityManager(entityManager);
        }
    }

    @Transactional
    public Iterator<Template> loadAll(){
        Iterable<Template> reportRules = templateRepository.findAll();
        if(reportRules != null){
            return reportRules.iterator();
        }
        return null;
    }

    @Transactional
    public List<ReportRuleTask> loadAllByRuleId(String ruleId){
        return reportRuleTaskRepository.findAllByRuleIdEquals(ruleId);
    }



}
