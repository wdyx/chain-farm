package com.report.doc;

import com.Constants;
import com.schedule.domain.Template;
import com.util.Utils;
import com.util.Word2Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * word工具类
 */
public class WordService extends BaseDoc {

    private static WordService wordService = null;

    public static WordService getInstance(Template template) {
        if (wordService == null) {
            wordService = new WordService(template);
        }
        return wordService;
    }

    private WordService(Template template) {
        this.docName = Constants.word_2007;
        this.template = template;
    }

    private Word2Util word2Util = Word2Util.getInstance();

    /**
     * word文件文本-word展示
     */
    public String text;

    /**
     * 获取表达式-word解析
     */
    public List<Object> expList;

    @Override
    public void init() throws OpenXML4JException, XmlException, IOException {

        // 获取Word中所有文本
        String text = word2Util.reeadWord(template.getPath());
        this.text = text;

        // 获取表达式内容
        this.expList = this.getExp();

    }

    /**
     * 获取文件文本信息
     *
     * @return
     */
    @Override
    public String getText() {
        return this.text;
    }

    /**
     * 生成文件
     */
    @Override
    public String create() {
        // 解析表达式
        return null;
    }


    /**
     * 获取模板文件中表达式内容
     *
     * @param templatePath 模板文件路径
     * @return
     */
    private Map<String, Object> getExp(String templatePath) throws Exception {

        Map<String, Object> expData = word2Util.getExpData(templatePath);

        return null;
    }

    /**
     * 获取模板的表达式
     *
     * @return
     */
    private List<Object> getExp() {

        // 获取没一行的数据
        String[] rows = this.text.split("\n");
        // 获取表达式
        List<Object> expList = new LinkedList<>();
        Map<String, String> table = new LinkedHashMap<>();
        int preIndex = 0;
        int tableIndex = 0;
        int rowIndex = 0;
        for (int i = 0; i < rows.length; i++) {

            String row = rows[i];
            if (StringUtils.isNotBlank(row)) {

                String[] tableRows = row.split("\t");
                // 判断是否是表格
                if (tableRows.length > 1) {

                    // 连续相同的下标表示同一个表格
                    if (preIndex == (i - 1) && i > 0 || preIndex == 0) {

                        for (int j = 0; j < tableRows.length; j++) {
                            String trow = tableRows[j];
                            // 文本直接判断是否是表达式
                            if (Utils.isExp(trow)) {
                                table.put(tableIndex + "-" + rowIndex + "-" + j, trow);
                            }
                        }
                        // 当前表格行下标
                        rowIndex++;
                    } else {
                        expList.add(table);
                        // 表格列表
                        tableIndex++;
                        rowIndex = 0;
                        table = new LinkedHashMap<>();
                    }

                    // 记录table表格上一行的下标
                    preIndex = i;
                } else {
                    // 文本直接判断是否是表达式
                    if (Utils.isExp(row)) {
                        expList.add(row);
                    }
                }
            }
            // 判断最后一行
            if (i == rows.length - 1) {
                if (!table.isEmpty()) {
                    expList.add(table);
                }
            }
        }
        return expList;
    }
}
