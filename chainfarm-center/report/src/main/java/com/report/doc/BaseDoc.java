package com.report.doc;

import com.schedule.domain.Template;
import com.report.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 文档父类
 */
public abstract class BaseDoc {

    @Autowired
    protected TemplateRepository templateRepository;

    /** 模板信息 */
    protected Template template;

    /** 当前文件类型 */
    protected String docName;

    /**
     * 初始化当前报表信息
     */
    public abstract void init() throws Exception;

    /**
     * 获取文件文本信息
     * @return
     */
    public String getText(){
        if(this.docName.startsWith("excel")){

        }
        return null;
    }

    /**
     * 生成文件
     */
    public abstract String create() throws Exception;



}
