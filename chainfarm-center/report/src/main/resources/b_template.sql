/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.220
Source Server Version : 50723
Source Host           : 192.168.0.220:3306
Source Database       : cloud_backend_monitor

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-03-01 17:15:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_template
-- ----------------------------
DROP TABLE IF EXISTS `b_template`;
CREATE TABLE `b_template` (
  `id` varchar(32) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `cron` varchar(20) DEFAULT NULL,
  `info` text,
  `userIds` varchar(500) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `docCode` varchar(20) DEFAULT NULL,
  `createUserId` varchar(20) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `suffix` varchar(20) DEFAULT NULL COMMENT '文件后缀',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_template
-- ----------------------------
INSERT INTO `b_template` VALUES ('1', '4201', '4201-软件系统数据库日运行监控表--{dd}日', '4.xtrj\\4201-rjxtsjkryxjkb-mb1.xlsx', '30 * * * * ?', '', 'admin', '4201-软件系统数据库日运行监控表', '4111', 'admin', '2019-03-01 15:08:15', '2019-03-01 15:08:15', '.xlsx');
