/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.220
Source Server Version : 50723
Source Host           : 192.168.0.220:3306
Source Database       : cloud_backend_monitor

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-03-01 17:15:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_attachment
-- ----------------------------
DROP TABLE IF EXISTS `b_attachment`;
CREATE TABLE `b_attachment` (
  `id` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `path` varchar(200) NOT NULL,
  `suffix` varchar(10) NOT NULL,
  `docCode` varchar(30) NOT NULL,
  `extend` varchar(100) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ruleId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='巡检报告附件';
