package com.yx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages={"com.yx.module.*.dao"})
public class WXApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WXApiApplication.class, args);
    }
}
