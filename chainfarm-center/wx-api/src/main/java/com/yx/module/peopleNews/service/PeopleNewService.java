package com.yx.module.peopleNews.service;

import com.alibaba.fastjson.JSONObject;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

/**
 * 获取人民日报新闻信息
 */
public interface PeopleNewService {

    /**
     * 获取第一页地址
     * @param edition
     * @return
     * @throws MalformedURLException
     */
    public  String getNewsUrl(String edition) throws MalformedURLException;

    /**
     * 获取当前新闻列表和第x版标题列表
     * @return
     * @throws Exception
     */
    public Map<String,List<JSONObject>> getFirstNews(String edition)  throws Exception;

    /**
     * 获取标题内容
     * @param url
     * @return
     */
    public JSONObject getContent(String url) throws Exception;
}
