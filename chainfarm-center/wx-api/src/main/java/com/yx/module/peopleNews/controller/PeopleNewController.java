package com.yx.module.peopleNews.controller;

import com.alibaba.fastjson.JSONObject;
import com.yx.module.peopleNews.service.PeopleNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 人民日报读取接口
 */
@RestController
@RequestMapping("peopleNew")
public class PeopleNewController {


    @Autowired
    private PeopleNewService peopleNewService;

    /**
     * 获取当前新闻列表和第x版标题列表
     * @param edition
     * @return
     * @throws Exception
     */
    @RequestMapping("/list/{edition}")
    public Object getFirstNews(String edition) throws Exception {
        String newsUrl = peopleNewService.getNewsUrl(edition);
        return peopleNewService.getFirstNews(newsUrl);
    }

    /**
     * 获取内容
     * @param url
     * @return
     * @throws Exception
     */
    public JSONObject getContent(String url) throws Exception {
        return peopleNewService.getContent(url);
    }
}
