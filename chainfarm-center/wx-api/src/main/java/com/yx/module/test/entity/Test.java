package com.yx.module.test.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * ceshi lei
 **/
@Data
@Entity
@Table(name = "test")
public class Test {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;
}
