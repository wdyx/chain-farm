package com.yx.module.test.dao;

import com.yx.module.test.entity.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @ClassName: TestRepository
 * @Description: TODO
 * @Author: 王涛
 * @Date: 2018-11-29 20:09
 * @Version 1.0
 */
public interface TestRepository extends JpaSpecificationExecutor<Test>,JpaRepository<Test,
        String> {

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    public Page<Test> findAll(Pageable pageable);

}
