package com.yx.module.peopleNews.service;

import com.alibaba.fastjson.JSONObject;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.yx.util.receiveInterface.PageUtil.*;

/**
 * 获取人民日报新闻列表
 */
@Service
public class PeopleNewServiceImpl implements PeopleNewService {

    private static final String prefix = "http://data.people.com.cn";

    /**
     * 人民日报地址第一个参数：年月日（20190129），第二个参数：版次
     */
    private static final String url = prefix + "/rmrb/%s/%s";

    public static SimpleDateFormat sdf = null;

    /**
     * 获取当前新闻列表和第一版标题列表
     *
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, List<JSONObject>> getFirstNews(String url) throws Exception {

        Document document = getDocument(url);
        if (document == null) {
            throw new Exception("获取信息异常");
        }
        // 获取版次信息和标题信息

        // 标题列表
        List<JSONObject> tList = new LinkedList<>();
        // 当前版次列表
        List<JSONObject> bcList = new LinkedList<>();

        // 获取板次
        Elements bancYbBtn = document.getElementsByClass("banc_yb_btn");
        for (Element bc : bancYbBtn) {
            JSONObject bcObj = new JSONObject();
            String bchref = prefix + bc.attr("href");
            String text = bc.parent().select("h3").text();
            String dataHref = bc.parent().select("h3").select("a").attr("href");

            // System.out.println("bchref:" + bchref + "  bc:" + text);
            bcObj.put("bc", text);
            bcObj.put("dataHref", prefix + dataHref);
            bcObj.put("picHref", bchref);
            bcList.add(bcObj);
        }

        // 获取当前版次下的标题
        Elements titleList = document.getElementsByClass("title_list");

        // 获取版数和文件数
//            Elements info = titleList.select("div").select("span");
//
//            System.out.println(info.html());
//            for (Element infoSpan:info) {
//
//                String text = infoSpan.text();
//                System.out.println("span:"+text);
//
//            }

        // 获取标题
        Elements titles = titleList.select("ul").select("li").select("h3").select("a");

        for (Element title : titles) {
            // 当前标题对象
            JSONObject titleObj = new JSONObject();
            String text = title.text();
            String href = title.attr("href");
//                System.out.println(text);
//                System.out.println(prefix + href);
            titleObj.put("title", text);
            titleObj.put("href", prefix + href);
            tList.add(titleObj);
        }

        // 返回结果
        Map<String, List<JSONObject>> result = new HashMap<>();
        result.put("title", tList);
        result.put("bc", bcList);
        return result;
    }

    /**
     * 获取标题内容
     *
     * @param url
     * @return
     */
    @Override
    public JSONObject getContent(String url) throws Exception {
        Document document = getDocument(url);

        // sub标题
        String subtitle = "subtitle";
        // 标题
        String title = "title";
        // 当前内容标签  “【人民日报2019-01-30 第1版 要闻】”
        String info = "sha_left";
        //  内容
        String detail = "detail_con";
        // 图片地址
        String src = "src";

        JSONObject content = new JSONObject();
        content.put(subtitle, getDivTextByClass(document, subtitle));
        content.put(title, getDivTextByClass(document, title));
        content.put(info, getDivTextByClass(document, info));

        Elements elementsDetail = getElementsByClass(document, detail);
        Elements imgElements = elementsDetail.select("img");
        for (Element element : imgElements) {
            String imgSrc = prefix + element.attr(src);
            element.attr(src, imgSrc);
        }

        String text = elementsDetail.html();

        content.put(detail, text);

        return content;
    }

    /**
     * 更具class获取div中的内容
     *
     * @param className
     * @return
     */
    public static Elements getElementsByClass(Document document, String className) {
        Elements elementsByClass = document.getElementsByClass(className);
        if (elementsByClass != null) {
            return elementsByClass;
        }
        return null;
    }

    /**
     * 更具class获取div中的内容
     *
     * @param className
     * @return
     */
    public static String getDivTextByClass(Document document, String className) {
        Elements elementsByClass = getElementsByClass(document, className);
        if (elementsByClass != null) {
            return elementsByClass.text();
        }
        return "";
    }

    /**
     * 获取当日的地址
     *
     * @param edition
     * @return
     * @throws MalformedURLException
     */
    @Override
    public  String getNewsUrl(String edition) throws MalformedURLException {
        if (sdf == null) {
            sdf = new SimpleDateFormat("yyyMMdd");
        }
        // 获取当前日期
        String time = sdf.format(new Date());
        // 地址
        String newsUrl = String.format(url, time, edition);
        return newsUrl;
    }

    /**
     * 测试
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        PeopleNewServiceImpl pp = new PeopleNewServiceImpl();
        String url = pp.getNewsUrl("1");
         pp.getFirstNews(url);

//        pp.getContent("http://data.people.com.cn/rmrb/20190130/1/5a06736489804dd390bfb35c91dae442");
    }
}
