package com.yx.module.test.controller;

import com.yx.module.test.dao.TestDao;
import com.yx.module.test.dao.TestRepository;
import com.yx.module.test.entity.Test;
import com.yx.util.RequestPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: TestController
 * @Description: TODO
 * @Author: 王涛
 * @Date: 2018-11-29 20:11
 * @Version 1.0
 */
@RestController
@RequestMapping("/view/test")
public class TestController {

    @Autowired
    private TestDao testDao;

    @Autowired
    private TestRepository testRepository;

    @RequestMapping("/sql/all")
    public Object getSQLData(){
        return testDao.getAll();
    }

    @RequestMapping("/jpa/all")
    public List<Test> getJpaData(){
        List<Test> all = testRepository.findAll();
        return all;
    }

    @RequestMapping("/page")
    public Page<Test> getJpaPageData(int limit,int page){

        Page<Test> allByResTypeEquals = testRepository.findAll(PageRequest.of(page, limit, Sort
                .Direction.DESC,"id"));

        return allByResTypeEquals;
    }


}
