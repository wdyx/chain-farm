package com.yx.module.test.dao;

import com.yx.module.test.entity.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * @ClassName: TestDao
 * @Description: TODO
 * @Author: 王涛
 * @Date: 2018-11-29 20:03
 * @Version 1.0
 */
@Repository
public class TestDao {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    public Object getAll(){
        String sql = "select * from test";
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            Query query = entityManager.createNativeQuery(sql,Test.class);
            List<Test> resultList = query.getResultList();
            return resultList;
        } finally {
            entityManager.close();
        }
    }
}
