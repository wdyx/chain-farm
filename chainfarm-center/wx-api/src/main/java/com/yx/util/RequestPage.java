package com.yx.util;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @param <T>
 */
@Data
public class RequestPage<T> implements Serializable {


    private int count = 0;
    private String msg = "";
    private List<T> data;

    private int page;// 第几页

    private int size;// 长度

    private T params;// 参数


    public  RequestPage(){}

    public RequestPage(int page, int size){
       setPage(page);
       setSize(size);
    }

    public RequestPage(int page, int size,T t){
        setPage(page);
        setSize(size);
        params = t;
    }

    public static RequestPage of(int page, int size){
        return new RequestPage(page,size);
    }


}
