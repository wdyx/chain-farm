package com.yx.util.receiveInterface;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 */
public class PageUtil {

    public static Document getDocument(String url) throws Exception {
        HttpResponse response = HttpClient.getJson(url);
        if (response == null) {
            throw new Exception("获取资源信息错误！");
        }

        if (response.getStatusCode() == 200) {
            String str = response.getRspStr();
            Document document = Jsoup.parse(str);

            return document;
        }
        return null;
    }
}
