package com.yx.util.receiveInterface;

/**
 * Created by GL on 2016/12/26.
 */
public class HttpClient {
    public static final String GET = "GET";
    public static final String POST = "POST";

    public static HttpResponse getJson(String path) {        //传入URL地址获取JSON数据
        HttpClientHelper httpClientHelper = new HttpClientHelper();
        HttpResponse result = httpClientHelper.doHttp(path, GET, "utf-8", "", "60000", "application/json;charset=UTF-8");
        return result;
    }

}
